'use strict';

var uuidV4 = require('uuid/v4');
module.exports = {
    filestorage: {
        root: './images',
        acl: 'public-read',
        allowedContentTypes: ['image/jpg', 'image/jpeg', 'image/png'],
        maxFileSize: 10 * 1024 * 1024,
        getFilename: function(fileInfo) {
            return 'image-' + uuidV4() + '.png';
        }
    }
};