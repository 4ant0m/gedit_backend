/**
 * Created by 4ant0m on 04.01.17.
 */
var winston = require('winston'),
    OPTION  = {
	    levels: {
		    info: 1,
		    debug: 3,
		    warn: 2,
		    error: 0
	    },
	    colors: {
		    info: 'blue',
		    debug: 'green',
		    warn: 'yellow',
		    error: 'red'
	    }
    },
    logger  = new (winston.Logger)({
	    levels: OPTION.levels,
	    transports: [
		    new (winston.transports.Console)({
			    name: 'DEBUG',
			    level: 'debug',
			    timestamp: true,
			    colorize: true
		    })
		    ,
		    new (winston.transports.Console)({
			    name: 'WARN',
			    level: 'warn',
			    timestamp: true,
			    colorize: true
		    }),
		    new (winston.transports.Console)({
			    name: 'ERROR',
			    level: 'error',
			    timestamp: true,
			    colorize: true
		    })
		    /*new (winston.transports.File)({
		     name: 'error-file',
		     filename: 'error.log',
		     level: 'error'
		     })*/
	    ]
    });

winston.addColors(OPTION.colors);

module.exports = logger;
