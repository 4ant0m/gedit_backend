/**
 * Created by 4ant0m on 08.02.17.
 */
'use strict';
module.exports = function () {
	var module = require(process.argv[2]);
	process.on('message', function (msg) {
		var data;
		if (module[msg.name])
			data = module[msg.name](msg.data);
		process.send({name: msg.name, data: data});
	});
}();