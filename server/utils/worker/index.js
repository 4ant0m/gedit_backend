/**
 * Created by 4ant0m on 08.02.17.
 */
'use strict';

var childProcess = require('child_process'),
    WORKER       = `${__dirname}/worker.js`;

module.exports = class {
	constructor(module_path) {
		this.module_path = module_path;
		this.thread      = childProcess.fork(WORKER, [this.module_path]);
	}

	exec(data, cb) {
		this.thread.send({name: data.name, data: data.data});
		this.thread.on('message', (msg) => {
			if (msg.name === data.name) {
				cb(msg.data)
			}
		});
	}

	kill() {
		this.thread.kill('SIGINT');
	}
};
