/**
 * Created by user on 28/04/17.
 */
    'use strict';

var mailgun = require('mailgun-js');

class Mailgun {
    constructor(params) {
        this.apiKey = params.apiKey;
        this.domain = params.domain;
        this.client = mailgun({apiKey: this.apiKey, domain: this.domain});
        return this;
    }
}

module.exports = Mailgun;
