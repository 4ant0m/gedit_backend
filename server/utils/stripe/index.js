/**
 * Created by 4ant0m on 24.02.17.
 */

'use strict';

var stripe = require('stripe');

class Stripe {
	constructor(params) {
		this.key         = params.key;
		this.client      = stripe(this.key);
		return this.client
	}
}
module.exports = Stripe;

