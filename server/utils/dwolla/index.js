/**
 * Created by 4ant0m on 31.01.17.
 */

'use strict';

var dwolla = require('dwolla-v2');

function _getId(res) {
	var id = res.headers.get('location');
	id     = id.substr(id.lastIndexOf('/') + 1);
	return id;
}


class Dwolla {
	constructor(params) {
		this.key         = params.key;
		this.secret      = params.secret;
		this.environment = params.environment;
		this.client      = new dwolla.Client({
			key: this.key,
			secret: this.secret,
			environment: 'sandbox' // optional - defaults to production
		});
		this.apiUrl      = this.client.apiUrl;
		return this;
	}

	getToken() {
		return new Promise((resolve, reject) => {
			if (!this.token)
				this.client.auth.client().then((appToken) => {
					this.token = appToken;
					resolve(this.token)
				}).catch(reject);
			else
				resolve(this.token);
		})
	}

	_makeQuery(method, resource, data) {
		return new Promise((resolve, reject) => {
			this.getToken()
				.then(token => token[method](resource, data))
				.then(resolve)
				.catch(reject)
		});
	}

	getAccount() {
		return new Promise((resolve, reject) => {
			this._makeQuery('get', '/')
				.then(res => this.getResource(res.body, 'account'))
				.then(resolve)
				.catch(reject)
		})
	}

	refreshToken(token) {
		return new Promise((resolve, reject) => {
			var oldToken = new client.Token(token || this.token);
			client.auth.refresh(oldToken)
				.then((token) => {
					this.token = token;
					resolve(token);
				})
				.catch(reject)
		});
	}

	createCustomer(data) {
		return new Promise((resolve, reject) => {
			this._makeQuery('post', 'customers', data)
				.then(res => resolve(_getId(res)))
				.catch(reject)
		});
	}

	getCustomers() {
		return new Promise((resolve, reject) => {
			this._makeQuery('get', 'customers')
				.then((res) => resolve(res.body._embedded.customers))
				.catch(reject)
		})
	}

	getCustomer(id) {
		return new Promise((resolve, reject) => {
			this._makeQuery('get', `customers/${id}`)
				.then((res) => resolve(res.body))
				.catch(reject)
		})
	}

	updateCustomer(id, data) {
		return new Promise((resolve, reject) => {
			this._makeQuery('post', `customers/${id}`, data)
				.then((res) => resolve(res.body))
				.catch(reject)
		})
	}

	createFund(data) {
		return new Promise((resolve, reject) => {
			this._makeQuery('post', 'funding-sources', data)
				.then((res) => resolve(_getId(res)))
				.catch(reject)
		});
	}

	updateFund(id, data) {
		return new Promise((resolve, reject) => {
			this._makeQuery('post', `${id}/funding-sources`, data)
			.then(res => resolve(_getId(res)))
			.catch(reject)
		})
	}

	removeFund(id) {
		return new Promise((resolve, reject) => {
			this._makeQuery('post', `funding-sources/${id}`, {removed: true})
				.then((res) => resolve(res))
				.catch(reject)
		})
	}

	getFundBalance(id) {
		return new Promise((resolve, reject) => {
			this._makeQuery('get', `funding-sources/${id}/balance`)
				.then((res) => resolve(res.body))
				.catch(reject)
		})
	}

	getFunds(id) {
		return new Promise((resolve, reject) => {
			this._makeQuery('get', `accounts/${id}/funding-sources`)
				.then((res) => resolve(res.body._embedded['funding-sources']))
				.catch(reject)
		});
	}

	createCustomerFund(id, data) {
		return new Promise((resolve, reject) => {
			this._makeQuery('post', `customers/${id}/funding-sources`, data)
				.then((res) => resolve(_getId(res)))
				.catch(reject)
		});
	}

	updateCustomerFund(id, id_fund, data) {
		return new Promise((resolve, reject) => {
			this._makeQuery('post', `customers/${id}/funding-sources/${id_fund}`, data)
				.then((res) => resolve(_getId(res)))
				.catch(reject)
		})
	}

	removeCustomerFund(id, id_fund) {
		return new Promise((resolve, reject) => {
			this._makeQuery('post', `customers/${id}/funding-sources/${id_fund}`, {removed: true})
				.then((res) => resolve(_getId(res)))
				.catch(reject)
		})
	}

	getCustomerFunds(id) {
		return new Promise((resolve, reject) => {
			this._makeQuery('get', `customers/${id}/funding-sources`)
			.then(res => resolve(res.body._embedded['funding-sources']))
			.catch(reject);
		})
	}

	getResource(link, source) {
		if (typeof link == 'object') {
			link = link._links[source].href
		}
		return new Promise((resolve, reject) => {
			this._makeQuery('get', link)
				.then((res) => resolve(res.body))
				.catch(reject)
		});
	}

	createMassPayments(sourceId, destinations, metadata) {
		var requestBody = {
			_links: {
				source: {
					href: `${this.apiUrl}/funding-sources/${sourceId}`
				}
			},
			items: []
		};
		if (metadata)
			requestBody.metadata = metadata;

		requestBody.items = destinations.map((item) => {
			var destination = {
				_links: {
					destination: {
						href: `${this.apiUrl}/accounts/${item.accountId}`
					}
				},
				amount: {
					currency: 'USD',
						value: item.value
				}
			};
			if (item.metadata)
				destination.metadata = item.metadata;
		});

		return new Promise((resolve, reject) => {
			this._makeQuery('post', `mass-payments`, requestBody)
				.then((res) => resolve(_getId(res)))
				.catch(reject)
		});
	}

	createTransfer(sourceId, destinationId, amount) {
		var requestBody = {
			_links: {
				source: {
					href: `${this.apiUrl}/funding-sources/${sourceId}`
				},
				destination: {
					href: `${this.apiUrl}/customers/${destinationId}`
				}
			},
			amount: {
				currency: 'USD',
				value: amount
			},
			clearing: {
				destination: 'next-available'
			}

		};
		return new Promise((resolve, reject) => {
			this._makeQuery('post', `transfers`, requestBody)
				.then((res) => resolve(_getId(res)))
				.catch(reject)
		})
	}

	getTransfers(id_customer, option) {
		return new Promise((resolve, reject) => {
			this._makeQuery('get', `customers/${id_customer}/transfers`, option)
				.then((res) => resolve(res.body._embedded['transfers']))
				.catch(reject)
		});
	}

	getTransfer(id) {
		return new Promise((resolve, reject) => {
			this._makeQuery('get', `transfers/${id}`)
				.then((res) => resolve(res.body))
				.catch(reject)
		});
	}

	getTransferFees(id) {
		return new Promise((resolve, reject) => {
			this._makeQuery('get', `transfers/${id}/fees`)
				.then((res) => resolve(res.body))
				.catch(reject)
		});
	}

	getTransferFailure(id) {
		return new Promise((resolve, reject) => {
			this._makeQuery('get', `transfers/${id}/failure`)
				.then((res) => resolve(res.body))
				.catch(reject)
		});
	}

	cancelTransfer(id) {
		return new Promise((resolve, reject) => {
			this._makeQuery('post', `transfers/${id}`, {status: 'cancelled'})
				.then((res) => resolve(res.body))
				.catch(reject)
		});
	}
}

module.exports = Dwolla;

