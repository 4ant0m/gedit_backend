/**
 * Created by 4ant0m on 04.01.17.
 */
'use strict';
var elasticsearch = require('elasticsearch'),
    logger        = require('../logger');

module.exports    = class {
	constructor(config, option) {
		this.client          = new elasticsearch.Client({
			host: config.host + ':' + config.port,
			log: 'trace'
		});
		this.INDEX           = config.index;
		this.SIZE_LIMIT      = option.SIZE_LIMIT || 100;
		this.SEARCH_FIELDS   = option.SEARCH_FIELDS || [];
		this.EXCLUDES_FIELDS = option.EXCLUDES_FIELDS || [];
		this.GEOPOINT_FIELD  = option.GEOPOINT_FIELD || ["geopoint"];
		this.MAPPING_FIELDS  = option.MAPPING_FIELDS || {};
		this.TYPE            = option.TYPE;
		if (this.TYPE)
			this._putMapping();
		this.client.ping({
			requestTimeout: Infinity
		}, (error) => {
			if (error) {
				logger.error('elasticsearch cluster is down!');
			} else {
				logger.info('Elasticsearch started...');
			}
		});
	}

	_getField(ns_string, object) {
		var parts  = ns_string.split('.'),
		    parent = object;

		parts.forEach((item) => {
			if (!parent) return parent;
			parent = parent[item];
		});
		return parent;
	}

	_setField(ns_string, value, object) {
		var parts  = ns_string.split('.'),
		    parent = {},
		    child  = value;

		for (var length = parts.length - 1, i = length; i > 0; i--) {
			parent           = object[parts[i - 1]];
			parent[parts[i]] = child;
			child            = parent;
		}
		return object;
	}

	_putMapping(type, proprities) {
		var self       = this,
		    proprities = proprities || this.MAPPING_FIELDS;

		type = type || this.TYPE;

		return new Promise((resolve, reject) => {
			var body              = {};
			body[type]            = {};
			body[type].properties = proprities;

			var map                       = {
				"mappings": {}
			};
			map.mappings[type]            = {};
			map.mappings[type].properties = proprities;

			self.client.indices.create({
				index: self.INDEX,
				body: map
			}, function (err, resp, respcode) {
				self.client.indices.putMapping({index: self.INDEX, type: type, body: body}, function (result) {
					logger.info('Elasticsearch mapping' - result);
				})
			});
		})
	}

	syncData(data, type, cb) {
		var self     = this,
		    query    = [],
		    geopoint = this.GEOPOINT_FIELD;

		type = type || this.TYPE;

		for (var i = 0, length = data.length; i < length; i++) {
			var dataJSON     = data[i].toJSON(),
			    dataGeopoint = this._getField(geopoint, dataJSON);
			if (dataGeopoint)
				this._setField(geopoint, {lat: dataGeopoint.lat, lon: dataGeopoint.lng}, dataJSON);
			query.push({index: {_index: this.INDEX, _type: type, _id: data[i].id}});
			query.push(dataJSON);
		}

		this.client.bulk({
			body: query
		}, function (err, resp) {
			return cb(err, resp);
		});
	}

	search(option, cb) {
		var self             = this,
		    geopoint         = this.GEOPOINT_FIELD,
		    highlight_fields = {},
		    query            = {};

		query.bool        = {};
		query.bool.must   = {};
		query.bool.filter = [];

		self.SEARCH_FIELDS.forEach((item) => {
			highlight_fields[item] = {"fragment_size": 150, "number_of_fragments": 3}
		});

		if (option.query) {
			query.bool.must.multi_match = {
				"query": option.query,
				"fields": option.SEARCH_FIELDS || self.SEARCH_FIELDS,
				"fuzziness": 'AUTO'
			}
		}
		else
			query.bool.must.match_all = {};

		if (option.size > self.SIZE_LIMIT) option.size = self.SIZE_LIMIT;

		if (option.terms)
			query.bool.filter = query.bool.filter.concat(option.terms);

		if (option.topLeft.lat || option.topLeft.lng || option.topLeft.lat || option.bottomRight.lat || option.bottomRight.lng) {
			var geo_bounding                        = {
				geo_bounding_box: {}
			};
			geo_bounding.geo_bounding_box[geopoint] = {
				"top_left": {
					"lat": option.topLeft.lat,
					"lon": option.topLeft.lng
				},
				"bottom_right": {
					"lat": option.bottomRight.lat,
					"lon": option.bottomRight.lng
				}
			};
			query.bool.filter.push(geo_bounding);
		}

		this.client.search({
			index: self.INDEX,
			type: option.type || self.TYPE,
			body: {
				"_source": {
					"excludes": option.EXCLUDES_FIELDS || self.EXCLUDES_FIELDS
				},
				size: option.size || 100,
				from: option.from || 0,
				query: query,
				aggs: option.aggs || {
					"area": {
						"geohash_grid": {
							"field": geopoint,
							"precision": 5
						},
						"aggs": {
							"cell": {
								"geo_bounds": {
									"field": geopoint
								}
							}
						}
					}
				},
				"highlight": {
					"order": "score",
					"fields": highlight_fields
				}
			}
		}, function (err, resp) {
			var hits = resp;
			cb(err, hits)
		});
	}
}



