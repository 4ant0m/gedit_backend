/**
 * Created by 4ant0m on 06.02.17.
 */

'use strict';
var Big        = require('big.js');
module.exports = class {
	static checkSystemMoneyBalance(data) {
		var accounts = data.accounts;
		var accountsBalance = new Big(0),
		    bonusBalance    = new Big(0),
		    systemBalance   = new Big(data.balance || 0),
		    result          = {};

		for (var i = 0, length = accounts.length; i < length; i++) {
			accountsBalance = accountsBalance.plus(accounts[i].balance || 0);
			bonusBalance    = bonusBalance.plus(accounts[i].bonus_balance || 0)
		}
		result.balance              = data.balance || 0;
		result.accountsBonusBalance = bonusBalance;
		result.accountsBalance      = accountsBalance;
		result.diff                 = systemBalance.minus(accountsBalance.plus(bonusBalance));

		return result;
	}

	static checkAccountsBalance(data) {
		var accounts = data.accounts;
		var result = [];
		for (var i = 0, length = accounts.length; i < length; i++) {
			var balanceSent     = new Big(0),
			    balanceReceived = new Big(0),
			    diff            = new Big(0),
			    account         = accounts[i];

			account["transactions_sent"].forEach((transaction) => {
				balanceSent = balanceSent.plus(transaction.cost);

			});
			account["transactions_received"].forEach((transaction) => {
				var receivedCost = new Big(transaction.cost);
					receivedCost = receivedCost.mul(1 - transaction.fee);
				balanceReceived = balanceReceived.plus(receivedCost);

			});
			diff                = balanceReceived.minus(balanceSent);
			result.push({
				accountId: account.id,
				balance: account.balance || 0,
				balanceSent: balanceSent,
				balanceReceived: balanceReceived,
				diff: diff,
				isValid: (diff.eq(account.balance || 0))
			})
		}
		return result;
	}
};

