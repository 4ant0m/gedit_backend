'use strict';
var logger = require('../utils').logger;
module.exports = {
	publish: function (socket, options) {
		var name;
		if (options) {
			var collectionName = options.collectionName,
			    method         = options.method,
			    data           = options.data,
			    modelId        = options.modelId;
			if (method === "POST" || modelId == null) {
				name = '/' + collectionName + '/' + method;
				socket.emit(name, data);
			}
			else {
				name = "/" + collectionName + '/' + modelId + '/' + method;
				socket.emit(name, data);
			}
			logger.debug(socket.id, "PUBLISH " + name);
		}
		else {
			throw 'Error: Option must be an object type';
		}
	},
	publishToAllSessions: function (app, clientId, options) {
		var tokens = app.client_tokens[clientId];

		for (var token in tokens) {
			if (!tokens.hasOwnProperty(token) || !app.token_socket[token])
				continue;
			this.publish(app.token_socket[token], options)
		}
	},
	isEmpty: function (obj) {
		var hasOwnProperty = Object.prototype.hasOwnProperty;
		if (obj == null)       return true;
		if (obj.length > 0)    return false;
		if (obj.length === 0)  return true;
		for (var key in obj) {
			if (this.hasOwnProperty.call(obj, key)) return false;
		}
		return true;
	},
	subscribe: function (socket, options, callback) {
		var name;
		if (options) {
			var collectionName = options.collectionName,
			    modelId        = options.modelId,
			    method         = options.method;
			if (method === 'POST' || modelId == null) {
				name = '/' + collectionName + '/' + method;
				socket.on(name, callback);
			}
			else {
				name = '/' + collectionName + '/' + modelId + '/' + method;
				socket.on(name, callback);
			}
			logger.debug(socket.id, "SUBSCRIBE " + name);
		}
		else {
			throw 'Error: Option must be an object';
		}
	}
};
