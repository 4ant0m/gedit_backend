/**
 * Created by 4ant0m on 08.12.16.
 */
'use strict';

var socket_io   = require('socket.io'),
    socket_auth = require('socketio-auth'),
    logger = require('../utils').logger;

module.exports = {

	init: function (app, server) {
		app.io = socket_io(server);
		app.socket_token = {};
		app.token_socket = {};
		app.client_tokens = {};

		socket_auth(app.io, {
			authenticate: function (socket, value, cb) {
				var AccessToken = app.models.AccessToken;
				var Account     = app.models.Account;
				var token       = AccessToken.find({
					where: {
						and: [{userId: value.userId}, {id: value.id}]
					}
				}, function (err, tokenDetail) {
					if (err) logger.error(err);
					logger.debug('token - ', tokenDetail);
					if (err) throw err;
					if (tokenDetail.length) {
						var token = tokenDetail[0];
						Account.findOne({
							where: {
								id: token.userId
							}
						}, (err, user) => {
							if (err) throw err;
							if (user) {
								app.socket_token[socket.id] = token;
								app.token_socket[token.id] = socket;

								if (!app.client_tokens[token.userId])
									app.client_tokens[token.userId] = {};
								app.client_tokens[token.userId][token.id] = token.id;

								app.emit('socket_authenticated', socket, {token: token, user: user});
								cb(null, true);
							} else
								cb(null, false);
						});
					} else {
						cb(null, false);
					}
				});
			}
		});

		app.on('socket_error', (socket, error) => {
			socket.emit('error', error);
		});
		app.io.on('connection', function (socket) {
			logger.debug(socket.id, ' - user connected');
			socket.on('disconnect', function () {
				logger.debug(socket.id, 'user disconnected');
				var token = app.socket_token[socket.id];
				if (token){
					delete app.client_tokens[token.userId][token.id];
					delete app.token_socket[token.id];
				}
					delete app.socket_token[socket.id];
				token = null;
				socket = null;
			});

			socket.on('error', function (data) {
				logger.error(data);
			});
		});

		app.emit('socket_init');
	}
};
