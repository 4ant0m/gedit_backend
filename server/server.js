'use strict';

var loopback    = require('loopback'),
    boot        = require('loopback-boot'),
    socket      = require('./socket/init.js'),
    path        = require('path'),
    fs          = require('fs'),
    preboot_dir = __dirname + '/preboot',
    logger      = require('./utils').logger;

var app = module.exports = loopback();

app.start = function () {
	// start the web server
	return app.listen(function () {
		app.emit('started');
		var baseUrl = app.get('url').replace(/\/$/, '');
		logger.info('Web server listening at: %s', baseUrl);
		if (app.get('loopback-component-explorer')) {
			var explorerPath = app.get('loopback-component-explorer').mountPath;
			logger.info('Browse your REST API at %s%s', baseUrl, explorerPath);
		}
	});
};

// Prebooting
function preboot(dirname, ctx) {
	fs.readdirSync(dirname).forEach(function (file) {
		if (file === 'index.js') return;
		require(path.join(dirname, file))(ctx);
	});
}
preboot(preboot_dir, loopback);
// Bootstrap the application, configure models, datasources and middleware.
// Sub-apps like REST API are mounted via boot scripts.

boot(app, __dirname, function (err) {
	if (err) throw err;
	// start the server if `$ node server.js`
	if (require.main === module) {
		socket.init(app, app.start());
	}
});
