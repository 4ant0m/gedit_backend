var should    = require('chai').should(),
    io        = require('socket.io-client'),
    supertest = require('supertest'),
    config    = require('./config.json'),
    api       = supertest('http://' + config.server + ':' + config.port + '/api');

var options = {
	log: true
};

const vendorHeader   = 'backdor1';
const customerHeader = 'backdor2';
const ORDER_STATUS   = {
	PENDING: 0,
	REJECTED: 1,
	CANCELED: 1,
	ACCEPTED: 2,
	ARRIVING: 3,
	ARRIVED: 4,
	FINISHED: 5
};

var log = function () {
};

if (options.log == true) {
	log = console.log
}

function OAuth(header) {
	if (!header)
		header = 'backdor1';
	return new Promise((resolve, reject) => {
		api.post('/Accounts/OAuth')
			.send({header: header})
			.end((err, res) => {
				if (err) throw err;
				resolve(res.body.token);
			});
	});
}

function socketAuth(header, cb) {
	var socket = io.connect('http://' + config.server + ':' + config.port);
	socket.on('connect', function () {
		OAuth(header).then((token) => {
			socket.on('authenticated', function () {
				console.log(token);
				cb(socket, token);
			});
			socket.emit('authentication', {
				id: token.id,
				userId: token.userId
			});
		})
	});
}

function createOrder(customerHeader, vendorHeader, serviceIds, cb) {
	OAuth(vendorHeader).then((vendorToken) => {
		OAuth(customerHeader).then((customerToken) => {
			api.post('/Accounts/' + customerToken.userId + '/customers' + '?access_token=' + customerToken.id + '')
				.send({
					"type": 0,
					"fee": 0,
					"vendorId": vendorToken.userId,
					"serviceIds": serviceIds
				})
				.end((err, res) => {
					log(JSON.stringify(res.body, "", 4));
					if (err) throw err;
					cb(res.body.id);
				})
		});
	});
}

function createOrderSocket(customerHeader, vendorHeader, serviceIds, cb) {
	OAuth(customerHeader).then((customerToken) => {
		OAuth(vendorHeader).then((vendorToken) => {
			socketAuth(customerHeader, (socket, token) => {
				socket.on(`/Account/${customerToken.userId}/customers/POST`, (data) => {
					log('order/create - ', JSON.stringify(data, "", 4));
					socket.removeAllListeners(`/Account/${customerToken.userId}/customers/POST`);
					return cb(data.id);
				});
				socket.emit('/Account/customers/POST', {
					id: customerToken.userId,
					data: {
						type: 0,
						fee: 0,
						vendorId: vendorToken.userId,
						serviceIds: serviceIds
					}
				})
			});
		});
	})
}

function acceptOrder(vendorHeader, orderId, cb) {
	socketAuth(vendorHeader, (socket, token) => {
		var semaphore = 1;
		socket.on('/Order/PATCH', (data) => {
			log('order/patch', data);
			socket.removeAllListeners('/Order/PATCH');
			if (semaphore == 0)
				return cb();
			semaphore -= 1;
		});

		socket.on('/Order/' + orderId + '/ACCEPT', (data) => {
			log('order/accept - ', data);
			data.should.to.have.property('type').equal(ORDER_STATUS.ACCEPTED);
			socket.removeAllListeners('/Order/' + orderId + '/ACCEPT');
			if (semaphore == 0)
				return cb();
			semaphore -= 1;
		});
		socket.emit('/Order/ACCEPT', {id: orderId});
	})
}

function setOnDutyAccountCategory(vendorHeader, id, onDuty, cb) {
	socketAuth(vendorHeader, (socket, token) => {
		socket.on('/AccountCategory/' + id + '/PATCH', (data) => {
			log('accountcategory - ', data);
			data.should.to.have.property('onDuty').equal(onDuty);
			socket.removeAllListeners('/AccountCategory/' + id + '/PATCH');
			cb();
		});
		socket.emit('/AccountCategory/ONDUTY', {id: id, onDuty: onDuty});
	});
}

function updateAccount(header, data, cb) {
	socketAuth(header, (socket, token) => {
		socket.on(`/Account/${token.userId}/PATCH`, (data) => {
			log('account/patch - ', data);
			socket.removeAllListeners(`/Account/${token.userId}/PATCH`);
			cb(data);
		});
		socket.emit(`/Account/PATCH`, {id: token.userId, data: data})
	})
}

describe("Vendor socket authentication", function () {
	it('errors if cannot authentication', function (done) {
		socketAuth(vendorHeader, (socket, token) => {
			done();
		});
	});
});

describe("Vendor socket AccountCategory", function () {
	it('errors if cannot set onDuty to false AccountCategory', function (done) {
		setOnDutyAccountCategory(vendorHeader, 1, false, done);
	});

	it('errors if cannot set onDuty to true AccountCategory', function (done) {
		setOnDutyAccountCategory(vendorHeader, 1, true, done);
	});
});

describe("Update account", function () {
	it('Update vendor#geopoint', function (done) {
		updateAccount(vendorHeader, {
			"geopoint": {
				"lat": 12.5,
				"lng": 14.6
			}
		}, (data) => {
			data.should.to.have.deep.property('geopoint.lat', 12.5);
			data.should.to.have.deep.property('geopoint.lng', 14.6);
			done();
		})
	});
	it('Update customer#mail', function (done) {
		var mail = `backdor${Math.random()}@test.com`;
		updateAccount(customerHeader, {
			"mail": mail
		}, (data) => {
			data.should.to.have.property('mail').equal(mail);
			done();
		})
	});
	it('Update customer#first_name', function (done) {
		var first_name = `First${customerHeader}`;
		updateAccount(customerHeader, {
			"first_name": first_name
		}, (data) => {
			data.should.to.have.property('first_name').equal(first_name);
			done();
		})
	});
	it('Update customer#balance', function (done) {
		var first_name = `First${customerHeader}`;
		updateAccount(customerHeader, {
			"balance": 14
		}, (data) => {
			data.should.to.have.property('balance').not.equal(14);
			done();
		})
	});
	it('Update customer#last_name', function (done) {
		var last_name = `Last${customerHeader}`;
		updateAccount(customerHeader, {
			"last_name": last_name
		}, (data) => {
			data.should.to.have.property('last_name').equal(last_name);
			done();
		})
	});
	it('Update vendor#first_name', function (done) {
		var first_name = `First${vendorHeader}`;
		updateAccount(vendorHeader, {
			"first_name": first_name
		}, (data) => {
			data.should.to.have.property('first_name').equal(first_name);
			done();
		})
	});
	it('Update vendor#last_name', function (done) {
		var last_name = `Last${vendorHeader}`;
		updateAccount(vendorHeader, {
			"last_name": last_name
		}, (data) => {
			data.should.to.have.property('last_name').equal(last_name);
			done();
		})
	});
	it('Update vendor#mail', function (done) {
		var mail = `backdor${Math.random()}@test.com`;
		updateAccount(vendorHeader, {
			"mail": mail
		}, (data) => {
			data.should.to.have.property('mail').equal(mail);
			done();
		})
	});
});

describe("Customer create credit card", function () {
	it('errors if cannot create credit card', function (done) {
		var credit_card = {
			title: 'test_card',
			exp_month: 10,
			exp_year: 2018,
			number: '4242 4242 4242 4242',
			cvc: 100
		};
		socketAuth(customerHeader, (socket, token) => {
			socket.on(`/Account/${token.userId}/cards/POST`, (data) => {
				log('creditcards - ', data);
				data.should.to.have.property('title', credit_card.title);
				data.should.to.have.property('number', credit_card.number);
				done();
			});
			socket.emit('/Account/cards/POST', {
				id: token.userId,
				data: credit_card
			});
		});
	});
});

describe("Vendor create fund", function () {
	it('errors if cannot create fund', function (done) {
		var fund = {
			"routingNumber": "222222226",
			"accountNumber": "123456789",
			"type": "checking",
			"name": "Test Bank"
		};
		socketAuth(vendorHeader, (socket, token) => {
			socket.on(`/Account/${token.userId}/funds/POST`, (data) => {
				log(`funds - `, data);
				data.should.to.have.property('name', fund.name);
                done();
			});
			socket.emit(`/Account/funds/POST`, {
				id: token.userId,
				data: fund
			})
		})
	});
});

describe('Socket Time/CURRENT', function () {
	it('errors if cannot get current time', function (done) {
		socketAuth(vendorHeader, (socket, token) => {
			socket.on('/Time/CURRENT', (data) => {
				log('time/CURRENT - ', data);
				socket.removeAllListeners('/Time/CURRENT');
				return done();
			});
			socket.emit('/Time/CURRENT', {});
		})
	});
});

describe("ORDER SOCKET", function () {
	describe("Vendor socket order", function () {
		var orderId = 1,
		    socket,
			token;

		it('errors if cannot auth vendor', function (done) {
			socketAuth(vendorHeader, (socket_connect, token_connect) => {
				socket = socket_connect;
				token = token_connect;
				return done();
			})
		});

		it('errors if cannot get order', function (done) {
			socket.on('/Order/POST', (data) => {
				log('order - ', data);
				socket.removeAllListeners('/Order/POST');
				return done();
			});
			createOrder(customerHeader, vendorHeader, [1], (id) => {
			});
		});

		it('errors if cannot accept order', function (done) {
			createOrder(customerHeader, vendorHeader, [1], (id) => {
				orderId = id;
				acceptOrder(vendorHeader, orderId, done);
			})
		});

		it('errors if cannot set order to arriving', function (done) {
			socket.on('/Order/' + orderId + '/ARRIVING', (data) => {
				log('order/arriving - ', data);
				socket.removeAllListeners('/Order/' + orderId + '/ARRIVING');
				return done();
			});

			var tomorrow = new Date();
			tomorrow.setDate(tomorrow.getDate() + 1);
			socket.emit('/Order/ARRIVING', {id: orderId, date: tomorrow});
		});

		it('errors if cannot set order to arrived', function (done) {
			socket.on('/Order/' + orderId + '/ARRIVE', (data) => {
				log('order/arrive - ', data);
				data.should.to.have.property('type').equal(ORDER_STATUS.ARRIVED);
				socket.removeAllListeners('/Order/' + orderId + '/ARRIVE');
				return done();
			});
			socket.emit('/Order/ARRIVE', {id: orderId});
		});

		it('errors if cannot start doing service', function (done) {
			socket.on('/Order/' + orderId + '/service/START', (data) => {
				log('order/service/start - ', data);
				data.should.to.have.property('start_time').not.equal(null);
				socket.removeAllListeners('/Order/' + orderId + '/service/START');
				return done();
			});
			socket.emit('/Order/service/START', {id: orderId, serviceId: 1});
		});

		it('errors if cannot finish doing service', function (done) {
			socket.on('/Order/' + orderId + '/service/FINISH', (data) => {
				log('order/service/finish - ', data);
				data.should.to.have.property('end_time').not.equal(null);
				socket.removeAllListeners('/Order/' + orderId + '/service/FINISH');
				return done();
			});
			socket.emit('/Order/service/FINISH', {id: orderId, serviceId: 1});
		});

		/*it('errors if cannot set order to finished', function (done) {
			socket.on('/Order/' + orderId + '/FINISH', (data) => {
				log('order/finish - ', data);
				socket.removeAllListeners('/Order/' + orderId + '/FINISH');
				return done();
			});
			socket.emit('/Order/FINISH', {id: orderId});
		});*/

		it('errors if customer cannot review order', function (done) {
			socketAuth(customerHeader, (socket, token) => {
				var review = {
					"rate": 4.5,
					"description": "good work_test"
				};
				socket.on('/Order/' + orderId + '/review/POST', (data) => {
					log('order/review - ', data);
					socket.removeAllListeners('/Order/' + orderId + '/review/POST');
					return done();
				});
				socket.emit('/Order/review/POST', {id: orderId, data: review});
			});
		});

		it('errors if cannot get orders cost', function (done) {
			socket.on('/Order/' + orderId + '/COSTS', (data) => {
				log('order/costs - ', data);
				var services = data.order,
				    cost     = 0;
				services.forEach((item) => {
					cost += item.service.cost;
				});
				data.should.to.have.property('cost').equal(cost.toFixed(2));
				socket.removeAllListeners('/Order/' + orderId + '/COSTS');
				return done();
			});
			socket.emit('/Order/COSTS', {id: orderId});
		});

		it('errors if customer cant pay for order', function (done) {
			socketAuth(customerHeader, (socket, token) => {
				socket.on('/Order/' + orderId + '/PAY', (data) => {
					log('order/pay - ', data);
					socket.removeAllListeners('/Order/' + orderId + '/PAY');
					return done();
				});
				socket.emit('/Order/PAY', {id: orderId})
			});
		});

		it('errors if vendor cant withdraw money', function (done) {
			socket.on(`/Account/${token.userId}/money/WITHDRAW`, (data) => {
				log('account/withdraw - ', data);
				socket.removeAllListeners(`/Account/${token.userId}/money/WITHDRAW`);
				return done();
			});
			socket.emit(`/Account/money/WITHDRAW`, {id: token.userId, amount: 1});
		})
	});

	describe("Customer socket order", function () {
		var socket;
		it('errors if cannot auth customer', function (done) {
			socketAuth(customerHeader, (socket_connect, token) => {
				socket = socket_connect;
				done();
			})
		});
		it('errors if customer cant cancel order', function (done) {
			setOnDutyAccountCategory(vendorHeader, 1, true, () => {
				createOrderSocket(customerHeader, vendorHeader, [1, 1], (orderId) => {
					acceptOrder(vendorHeader, orderId, () => {
						socket.on('/Order/' + orderId + '/CANCEL', (data) => {
							log('order/cancel - ', data);
							socket.removeAllListeners('/Order/' + orderId + '/CANCEL');
							return done();
						});
						socket.emit('/Order/CANCEL', {id: orderId});
					})
				})
			})
		})
	});
});




