/** For run tests drop database (in config.json 'force_migrate: true')*/

var should    = require('chai').should(),
    supertest = require('supertest'),
    config    = require('./config.json'),
    api       = supertest('http://' + config.server + ':' + config.port + '/api');

var log = function(){};
log = console.log;

function OAuth(header) {
	if (!header)
		header = 'backdor1';
	return new Promise((resolve, reject) => {
		api.post('/Accounts/OAuth')
			.send({header: header})
			.end((err, res) => {
				if (err) throw err;
				log('token - ', res.body.token);
				resolve(res.body.token);
			})
	});
}

describe('AUTHORIZATION /Accounts/OAuth', function () {
	it('errors if cant recieve correct header', function (done) {
		api.post('/Accounts/OAuth')
			.send({header: 'backdor1'})
			.expect(function (res) {
				res.body.should.to.have.property('token')
			})
			.end(function (err, res) {
				if (err) throw err;
				done();
			});
	});

	it('errors if cant recieve incorrect header', function (done) {
		api.post('/Accounts/OAuth')
			.send({header: 'incorrect'})
			.expect(function (res) {
				res.body.should.to.have.deep.property('error.message', 'Invalid header')
			})
			.end(function (err, res) {
				if (err) throw err;
				done();
			});
	});
});

describe('UPDATE /Accounts/', function () {
	it('errors if has permission', function (done) {
		api.put('/Accounts')
			.query({'access_token': 'dads'})
			.send({})
			.expect((res) =>
				res.body.should.to.have.deep.property('error.code', 'AUTHORIZATION_REQUIRED')
		)
			.end(function (err, res) {
				if (err) throw err;
				done();
			});
	});

	it('errors if cannot update', function (done) {
		api.post('/Accounts/OAuth')
			.send({header: 'backdor1'})
			.end((err, res) => {
				var token = res.body.token;
				api.patch('/Accounts/' + token.userId + '?access_token=' + token.id + '')
					.send({mail: "backdor1@test.com"})
					.expect((res) => {
						log('update/rest - ', res.body);
						res.body.should.to.have.property('mail').equal('backdor1@test.com')
					})
					.end(function (err, res) {
						if (err) throw err;
						done();
					})
			});
	})
});

describe('CATEGORIES /Category/ && Accounts/categories/rel/', function () {
	var categoryId = 1;
	var serviceId = 1;
	it('errors if cannot create category', function (done) {
		OAuth('backdor1').then((token) => {
			api.post('/Category/' + '?access_token=' + token.id + '')
				.send({
					"name": "Category_TEST",
					"type": 0,
					"image": "../img/test.png"
				})
				.expect((res) => {
					res.body.should.not.to.have.property('error');
				})
				.end((err, res) => {
					if (err) throw err;
					categoryId = res.body.id;
					done();
				})
		}).catch((err) => done(err));
	});

	it('errors if cannot put category to account', function (done) {
		OAuth('backdor1').then((token) => {
			api.put('/Accounts/' + token.userId + '/categories/rel/'+ categoryId + '?access_token=' + token.id + '')
				.send({
					"onDuty": true
				})
				.expect((res) => {
					log('category - ', res.body);
					res.body.should.not.to.have.property('error');
				})
				.end((err, res) => {
					if (err) throw err;
					done();
				})
		}).catch((err) => done(err));
	});

	describe('SERVICES /Accounts/services', function () {
		it('errors if cannot create service 1', function (done) {
			OAuth('backdor1').then((token) => {
				api.post('/Accounts/' + token.userId + '/services'+'?access_token=' + token.id + '')
					.send({
						"title": "test_service",
						"description": "test_description",
						"cost": 20.2,
						"cost_type": 0,
						"categoryId": categoryId,
						"accountId": token.userId
					})
					.expect((res) => {
						res.body.should.not.to.have.property('error');
					})
					.end((err, res) => {
						if (err) throw err;
						serviceId = res.body.id;
						done();
					})
			}).catch((err) => done(err));
		});

		it('errors if cannot update service 1', function (done) {
			OAuth('backdor1').then((token) => {
				api.put('/Accounts/' + token.userId + '/services/'+ serviceId + '?access_token=' + token.id + '')
					.send({
						"cost": 40.4,
						"title": "test_service_updated"
					})
					.expect((res) => {
						log('service - ', res.body);
						res.body.should.to.have.property('cost').equal(40.4);
						res.body.should.to.have.property('title').equal("test_service_updated");
					})
					.end((err, res) => {
						if (err) throw err;
						done();
					})
			}).catch((err) => done(err));
		});
	});

	describe('ORDER /Account/customers', function () {
		var orderId = 1;
		it('errors if cannot create order 1', function (done) {
			OAuth('backdor2').then((token) => {
				api.post('/Accounts/' + token.userId + '/customers' + '?access_token=' + token.id + '')
					.send({
						"fee": 0,
						"vendorId": 1,
						"serviceIds": [serviceId]
					})
					.expect((res) => {
						log('order - ', res.body);
						res.body.should.not.to.have.property('error');
					})
					.end((err, res) => {
						if (err) throw err;
							orderId = res.body.id;
						done();
					})
			}).catch((err) => done(err));
		});

		it('errors if cannot get new orders', function (done) {
			OAuth('backdor1').then((token) => {
				api.get('/Orders/new' + '?access_token=' + token.id + '')
					.send({})
					.expect((res) => {
						log(res.body);
						res.body.should.to.have.property('orders').and.to.not.be.a('null');
					})
					.end((err, res) => {
						if (err) throw err;
						return done();
					})
			}).catch((err) => done(err));
		});

		it('errors if cannot reject new order 1', function (done) {
			OAuth('backdor1').then((token) => {
				api.put('/Orders/' + orderId +'/reject'+'?access_token=' + token.id + '')
					.send({})
					.expect((res) => {
						log(res.body);
						res.body.should.not.to.have.property('error');
					})
					.end((err, res) => {
						if (err) throw err;
						return done();
					})
			}).catch((err) => done(err));
		});

		it('errors if cannot create order 2', function (done) {
			OAuth('backdor2').then((token) => {
				api.post('/Accounts/' + token.userId + '/customers' + '?access_token=' + token.id + '')
					.send({
						"fee": 0,
						"vendorId": 1,
						"serviceIds": [serviceId]
					})
					.expect((res) => {
						log('order - ', res.body);
						res.body.should.not.to.have.property('error');
					})
					.end((err, res) => {
						if (err) throw err;
						orderId = res.body.id;
						done();
					})
			}).catch((err) => done(err));
		});

		it('errors if cannot accept new order 2', function (done) {
			OAuth('backdor1').then((token) => {
				api.put('/Orders/' + orderId +'/accept'+'?access_token=' + token.id + '')
					.send({})
					.expect((res) => {
						log(res.body);
						res.body.should.not.to.have.property('error');
					})
					.end((err, res) => {
						if (err) throw err;
						return done();
					})
			}).catch((err) => done(err));
		});

		it('errors if cannot cancel new order 2', function (done) {
			OAuth('backdor1').then((token) => {
				api.put('/Orders/' + orderId +'/cancel'+'?access_token=' + token.id + '')
					.send({})
					.expect((res) => {
						log(res.body);
						res.body.should.not.to.have.property('error');
					})
					.end((err, res) => {
						if (err) throw err;
						return done();
					})
			}).catch((err) => done(err));
		})
	});
});
