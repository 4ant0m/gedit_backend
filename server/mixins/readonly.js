
module.exports = function(Model, options) {
    'use strict';

    // Make sure emailVerified is not set by creation
    Model.stripReadOnlyProperties = function(ctx, modelInstance, next) {
        if (ctx.options && ctx.options.skipPropertyFilter) return next();
        var body = ctx.data || {},
            instance = ctx.instance || ctx.currentInstance;

        var properties = (Object.keys(options).length) ? options : null;
        if (properties) {
            Object.keys(properties).forEach(function(key) {
                if (instance) {
                    body[key] = instance[key];
                }
                else
                delete body[key];
            });
            next();
        } else {
            var err = new Error('Unable to update: ' + Model.modelName + ' is read only.');
            err.statusCode = 403;
            next(err);
        }
    };


    // Make sure emailVerified is not set by creation

   /* Model.beforeRemote('create', function(ctx, modelInstance, next) {
        Model.stripReadOnlyProperties(ctx, modelInstance, next);
    });
    Model.beforeRemote('upsert', function(ctx, modelInstance, next) {
        Model.stripReadOnlyProperties(ctx, modelInstance, next);
    });
    Model.beforeRemote('prototype.updateAttributes', function(ctx, modelInstance, next) {
        Model.stripReadOnlyProperties(ctx, modelInstance, next);
    });
    Model.beforeRemote('prototype.updateAttribute', function(ctx, modelInstance, next) {
        Model.stripReadOnlyProperties(ctx, modelInstance, next);
    });*/
    Model.observe('before save', function(ctx, next) {
        Model.stripReadOnlyProperties(ctx, null, next);
    });

};