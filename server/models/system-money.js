/**
 * Created by 4ant0m on 25.01.17.
 */
'use strict';
var logger       = require('../utils').logger;
var CHECKBALANCE = `${__dirname}/../workers/check-balance.js`;
var Worker       = require('../utils').worker;

module.exports = function (SystemMoney) {
	//hide remote orders method
	SystemMoney.disableRemoteMethod("upsert", true);
	//SystemMoney.disableRemoteMethod("updateAll", true);
	//SystemMoney.disableRemoteMethod("updateAttributes", false);
	SystemMoney.disableRemoteMethod("deleteById", true);
	SystemMoney.disableRemoteMethod("exists", true);

	SystemMoney.on('attached', () => {
		SystemMoney.create({id: 1, balance: 0}, (err, result) => {
			return logger.info('Init SystemMoney', result)
		})
	});


	SystemMoney.prototype.fullCheckBalance = function (cb) {
		var self    = this,
		    results = [];
		SystemMoney.checkAccountsBalance((err, result) => {
			if (err) results.push({err: err, AccountsBalance: result});
			else
				results.push({err: err, AccountsBalance: result});
			self.checkSystemMoneyBalance((err, result) => {
				if (err) results.push({err: err, SystemBalance: result});
				else
					results.push({err: err, SystemBalance: result});
				cb(null, results)
			})
		})
	};

	SystemMoney.checkAccountsBalance = function (cb) {
		var Account = SystemMoney.app.models.Account,
		    worker  = new Worker(CHECKBALANCE);

		Account.find({include: ['transactions_sent', 'transactions_received']}, (err, accounts) => {
			if (err && !accounts.length) return cb(err, accounts);
			worker.exec({name: 'checkAccountsBalance', data: {accounts: accounts}}, (result) => {
				worker.kill();
				cb(err, result)
			});
		});
	};

	SystemMoney.prototype.checkSystemMoneyBalance = function (cb) {
		var Account = SystemMoney.app.models.Account,
		    worker  = new Worker(CHECKBALANCE);

		Account.find({}, (err, accounts) => {
			if (err && !accounts.length) return cb(err, accounts);

			worker.exec({
				name: 'checkSystemMoneyBalance',
				data: {accounts: accounts, balance: this.balance}
			}, (result) => {
				worker.kill();
				cb(err, result)
			});
		});
	};

	SystemMoney.remoteEndpoint(
		'fullCheckBalance',
		{
			isStatic: false,
			http: {path: '/fullCheckBalance', verb: 'GET'},
			accepts: [],
			returns: {type: 'object', root: true}
		}
	);

	SystemMoney.remoteEndpoint(
		'checkAccountsBalance',
		{
			http: {path: '/checkAccountsBalance', verb: 'GET'},
			accepts: [],
			returns: {type: 'object', root: true}
		}
	);

	SystemMoney.remoteEndpoint(
		'checkSystemMoneyBalance',
		{
			isStatic: false,
			http: {path: '/checkSystemMoneyBalance', verb: 'GET'},
			accepts: [],
			returns: {type: 'object', root: true}
		}
	);
};