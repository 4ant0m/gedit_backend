'use strict';

module.exports = function (Service) {
	Service.validatesPresenceOf('title', 'categoryId', 'accountId');

	Service.observe('before save', function logQuery(ctx, next) {
		var instance = ctx.instance || ctx.currentInstance || ctx.data;

		Service.app.models.AccountCategory.find({
			where: {
				and: [
					{accountId: instance.accountId},
					{categoryId: instance.categoryId}
				]
			}
		}, (err, result) => {
			if (err) return next(err);
			if (result && result.length == 0) return next(new Error("Account doesn't have this category"));
			if (ctx.instance) {
				ctx.instance.account_categoryId = result[0].id;
			} else {
				ctx.data.account_categoryId = result[0].id;
			}
			next();
		})
	});

	Service.beforeRemote('prototype.uploadPic', function(ctx, modelInstance, next) {
		var service = ctx.instance,
			Order = Service.app.models.Order;

			Order.active(ctx, (err, order) => {
			if (order && order.serviceIds.indexOf(service.id) !== -1) {
				next();
			} else {
				next(new Error('Order is inactive'))
			}
		});
	});

	Service.prototype.uploadPic = function (ctx, options, cb) {
		if (!options) options = {};
        var ServiceImage = Service.app.models.ServiceImage,
			self = this;
        ServiceImage.upload(ctx, options, self.id, cb)
	};

	Service.remoteMethod(
		'uploadPic',
		{
			isStatic: false,
			http: {path: '/upload/image', verb: 'POST'},
			accepts: [
				{arg: 'ctx', type: 'object', 'http': {source: 'context'}},
				{arg: 'option', type: 'object'}
			],
			returns: {type: 'object', root: true}
		}
	);
};
