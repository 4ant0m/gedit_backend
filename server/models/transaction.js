'use strict';

var logger = require('../utils').logger;

module.exports = function (Transaction) {
    Transaction.disableRemoteMethod("upsert", true);
    Transaction.disableRemoteMethod("updateAll", true);
    Transaction.disableRemoteMethod("updateAttributes", false);

    Transaction.disableRemoteMethod("deleteById", true);
    Transaction.disableRemoteMethod("exists", true);

    // --- definitions ---
    //Transaction.definition.properties.type.default = 0;

    // --- vaildations ---
    Transaction.validatesPresenceOf('recipientId', 'cost');

    Transaction.observe('before save', function (ctx, next) {
        var transaction = ctx.instance;

        if (!transaction || transaction.orderId == null || transaction.senderId == null) {
            return next();
        }
        if (transaction)
            transaction.check(next)
    });

    Transaction.prototype.check = function (cb) {
        var Order = Transaction.app.models.Order,
            transaction = this;
        Order.find({
            where: {
                and: [
                    {id: transaction.orderId},
                    {or: [{vendorId: transaction.recipientId}, {customerId: transaction.senderId}]}
                ]
            }
        }, (err, orders) => {
            if (!err && orders.length) {
                orders[0].costs((err, result) => {
                    logger.debug(result, transaction);
                    if (result.cost != transaction.cost.toFixed(2))
                        return cb(err || new Error('Invalid count, might be ' + result.cost));
                    cb();
                });
            } else {
                return cb(err || new Error('Invalid order'))
            }
        })
    };

    Transaction.prototype.send = function (cxt, cb) {
        var SystemMoney = Transaction.app.models.SystemMoney;
        cb = cb || (() => {
        });
        if (this.type !== 0) cb(new Error('Transaction was sent already'));
        this.type = 1;
        this.systemMoneyId = 1;
        this.save((err, result) => {
            if (err || !result) return cb(err);
            changeSenderBalance.bind(this)((err, res) => {
                if (err) return cb(err);
                if (res) changeRecipientBalance.bind(this)(this.fee);
                else  changeRecipientBalance.bind(this)(0);
                SystemMoney.changeSystemMoneyBalance();
            });

            function changeSenderBalance(cb) {
                this.sender((err, sender) => {
                    if (err) return cb(err);
                    if (sender && this.senderId != null) {
                        sender.changeAccountBalance(null, -1 * this.cost, 0, (err, res) => {
                            if (err) return cb(err);
                            return cb(err, res);
                        });
                    } else {
                        return cb(null, null);
                    }
                });
            }

            function changeRecipientBalance(fee, cb) {
                this.recipient((err, recipient) => {
                    if (err || !recipient) return cb(err);
                    recipient.changeAccountBalance(null, this.cost, fee, (err, res) => {
                        if (err) return cb(err);
                        return cb(null, result);
                    });
                });
            }
        });

    };

    Transaction.remoteEndpoint(
        'send',
        {
            http: {path: '/send', verb: 'PUT'},
            isStatic: false,
            accepts: [
                {arg: 'ctx', type: 'object', 'http': {source: 'context'}}
            ],
            returns: {arg: 'transaction', type: 'object'}
        }
    );
};
