/**
 * Created by 4ant0m on 13.12.16.
 */
'use strict';
var pubsub = require('../socket/pubsub.js');

module.exports = function (ServiceOrder) {

	ServiceOrder.start = function(orderId, serviceId, time, cb) {
		ServiceOrder.findOne({where: {and: [{orderId: orderId}, {serviceId: serviceId}, {start_time: null}]} }, (err, result) => {
			if (err || !result) return cb(err, result);
			result.start_time = time;
			result.save((err, result) => {
				cb(err, result);
			})
		})
	};

	ServiceOrder.finish = function(orderId, serviceId, time, cb){
		ServiceOrder.findOne({where: {and: [{orderId: orderId}, {serviceId: serviceId}, {end_time: null}]} }, (err, result) => {
			if (err || !result) return cb(err, result);
			result.end_time = time;
			result.save((err, result) => {
				cb(err, result);
			})
		})
	};

	ServiceOrder.finishAll = function(orderId, cb){
		ServiceOrder.updateAll({orderId: orderId, end_time: null}, {end_time: new Date()}, cb);
	};

	ServiceOrder.observe('after save', function logQuery(ctx, next) {
		if (!ctx.instance) return next();
		var where = {
			    where: ctx.where ||
			    {
				    id: ctx.instance.id
			    },
			    include: ['order']
		    },
		app = ServiceOrder.app;
			ServiceOrder.findOne(where
				, (err, result) => {
				if (err || !result) return next(err, result);
				result = result.toJSON();

				var clientIds = [];
					clientIds.push(result.order.vendorId);
				    clientIds.push(result.order.customerId);

				clientIds.forEach((id) => {
					if (ctx.isNewInstance == true) {
						pubsub.publishToAllSessions(app, id, {
							collectionName: 'ServiceOrder',
							method: 'POST',
							data: ctx.instance
						})
					} else {
							pubsub.publishToAllSessions(app, id, {
								collectionName: 'ServiceOrder',
								method: 'PATCH',
								data: ctx.instance,
								modelId: ctx.instance.id
							})
					}
				});
				next();
		})
	});

};

