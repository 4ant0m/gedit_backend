'use strict';

var imagemin = require('imagemin'),
    path = require('path'),
    imageminGm = require('../utils').imageminGm; // need graphicsmagick


module.exports = function (Serviceimage) {
    Serviceimage.disableRemoteMethod("create", true);
    Serviceimage.disableRemoteMethod("updateAll", true);
    Serviceimage.disableRemoteMethod("updateAttributes", true);
    Serviceimage.disableRemoteMethod("updateById", true);
    Serviceimage.disableRemoteMethod("deleteById", true);
    Serviceimage.disableRemoteMethod("exists", true);
    Serviceimage.disableRemoteMethod("createChangeStream", true);
    Serviceimage.disableRemoteMethod("replaceOrCreate", true);
    Serviceimage.disableRemoteMethod("replaceById", true);
    Serviceimage.disableRemoteMethod("upsertWithWhere", true);

    Serviceimage.upload = function (ctx, options, serviceId, cb) {
        if (!options) options = {};
        ctx.req.params.container = 'service';
        var Container = Serviceimage.app.models.container;
        Container.upload(ctx.req, ctx.result, options, function (err, fileObj) {
            if (err) {
                cb(err);
            } else {
                var fileInfo = fileObj.files.file[0],
                    CONTAINERS_URL = '/api/containers/',
                    container = fileInfo.container,
                    containerMin = 'min' + fileInfo.container,
                    url = CONTAINERS_URL + container + '/download/' + fileInfo.name,
                    urlMin = CONTAINERS_URL + containerMin + '/download/' + fileInfo.name;

                Serviceimage.createMetaData({
                    serviceId: serviceId,
                    imagesDir: path.normalize(`${__dirname}/../../images/`),
                    container: container,
                    containerMin: containerMin,
                    name: fileInfo.name,
                    imageURL: url,
                    imageMinURL: urlMin,
                    width: 400,
                    height: 400
                }, cb);
            }
        });
    };

    Serviceimage.createMetaData = function (data, cb) {
        Serviceimage.zipImage(data, function (err, files) {
            if (err) return cb(err);
            Serviceimage.create({
                serviceId: data.serviceId,
                name: data.name,
                imageURL: data.imageURL,
                imageMinURL: data.imageMinURL
            }, cb)
        });

    };

    Serviceimage.zipImage = function (data, cb) {
        var imagesDir = data.imagesDir,
            container = data.container,
            containerMin = data.containerMin,
            name = data.name;

        imagemin([`${imagesDir}/${container}/${name}`], `${imagesDir}/${containerMin}`, {
            use: [
                imageminGm.crop({width: data.width, height: data.height, x: data.x, y: data.y})
            ]
        }).then(files => {
            cb(null, files)
        }).catch(cb)
    }

};
