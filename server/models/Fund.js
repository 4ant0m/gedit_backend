/**
 * Created by 4ant0m on 16.02.17.
 */
	'use strict';
var withdraw = require('../payment').withdraw;

module.exports = function (Fund) {

	Fund.observe('before save', function (ctx, next) {
		var fund = ctx.currentInstance;
		if (ctx.instance) return next();
		if (fund && fund.accountId == null)
			return withdraw.updateSystemFund(instance).then(res => {
				return next();
			}).catch(next);
		else
			next(new Error('Cannot update customer fund'));
	});

	Fund.observe('before save', function (ctx, next) {
		var Account  = Fund.app.models.Account,
		    instance = ctx.instance;
		if (!instance) return next();
		if (instance.accountId == null) {
			return withdraw.createSystemFund(instance).then(res => {
				ctx.instance.dwollaId = res;
				return next();
			}).catch(next);
		}

		Account.findById(instance.accountId, (err, user) => {
			if (err) return next(err);
			if (user) {
				withdraw.createDwollaUser(user).then(res => {
					console.log('fundDwolla',res);
						user.dwollaId = res;
						user.save();
						return withdraw.createCustomerFund(res, instance)
				}).then(res => {
					ctx.instance.dwollaId = res;
					return next()
				}).catch(err => {
					console.log('fundDwolla', err)
					next(err)
				});
			} else
				return next(new Error('User not found'));
		});
	});

	Fund.observe('before delete', function (ctx, next) {
		Fund.findById(ctx.where.id, (err, fund) => {
			if (err || !fund) return next(err);
			if (fund.accountId == null)
				return withdraw.removeSystemFund(fund.dwollaId).then(res => {
					next();
				}).catch(next);
			next(new Error('Cannot delete customer fund'))
		});
	});

	Fund.getDwollaFunds = function (ctx, accountId, cb) {
		var Account = Fund.app.models.Account;
		if (accountId)
			return Account.findById(accountId, (err, user) => {
				if (err || !user) cb(err);
				withdraw.getCustomerFunds(user.dwollaId)
					.then(res => cb(null, res))
					.catch(cb)
			});
		return withdraw.getFunds()
			.then(res => cb(null, res))
			.catch(cb)
	};

	Fund.getBalance = function (ctx, cb){
		withdraw.getBalance().then(res => cb(null, res)).catch(cb);
	};

	Fund.remoteMethod(
		'getDwollaFunds',
		{
			http: {path: '/getDwollaFunds', verb: 'GET'},
			accepts: [
				{arg: 'ctx', type: 'object', 'http': {source: 'context'}},
				{arg: 'accountId', type: 'number'}],
			returns: {type: 'object', root: true}
		}
	);

	Fund.remoteMethod(
		'getBalance',
		{
			http: {path: '/getFundBalance', verb: 'GET'},
			accepts: [{arg: 'ctx', type: 'object', 'http': {source: 'context'}}],
			returns: {type: 'object', root: true}
		}
	)
};

