'use strict';
var pubsub = require('../socket/pubsub.js');
var request = require('request');
var CHECKBALANCE = `${__dirname}/../workers/check-balance.js`;
var Worker = require('../utils').worker;
var withdraw = require('../payment').withdraw;
var Big = require('big.js');

module.exports = function (Account) {
    Account.disableRemoteMethod('__create__orders', false);
    Account.disableRemoteMethod('__delete__orders', false);
    Account.disableRemoteMethod('__destroyById__orders', false);
    Account.disableRemoteMethod('__updateById__customers', false);

    Account.disableRemoteMethod('__delete__customers', false);
    Account.disableRemoteMethod('__destroyById__customers', false);
    Account.disableRemoteMethod('__updateById__customers', false);

    // -- validations --
    delete Account.validations.email;
    Account.validatesUniquenessOf('mobile', {message: 'mobile is not unique'});
    Account.validatesUniquenessOf('mail', {message: 'mail is not unique'});

    // -- hooks --
    Account.beforeRemote('*.__get__categories', function (ctx, user, next) {
        var AccountCategory = Account.app.models.AccountCategory;
        AccountCategory.find({
            where: {
                accountId: ctx.req.accessToken.userId
            },
            include: {
                relation: 'category'
            }

        }).then((resutl) => {
            ctx.res.send(resutl);
        });
    });

    Account.observe('before save', function (ctx, next) {
        if (!ctx.instance) return next();
        ctx.instance.publishGeoWatch();
        next();
    });

    // --- Account's methods ---
    function getAccessToken(user, cb) {
        user.createAccessToken(60000 * 60 * 24 * 30, function (err, token) {
            if (err) return cb(err);
            cb(null, token);
        })
    }

    function loginBackdor(mobile, cb) {
        Account.loginOrCreateByPhone(mobile, (err, result) => {
            if (err) return cb(err, result);
            cb(null, result)
        })
    }

    Account.OAuth = function (ctx, body, cb) {
        var header = body.header,
            options = {
                url: 'https://api.digits.com/1.1/sdk/account.json',
                headers: {
                    'Authorization': header
                }
            };

        if (header.indexOf('backdor') !== -1)
            return loginBackdor(header, cb);

        request(options, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                var mobile = JSON.parse(body).phone_number;
                Account.loginOrCreateByPhone(mobile, (err, result) => {
                    if (err) return cb(err, result);
                    cb(null, result);
                })
            }
            else {
                cb(new Error('Invalid header'), null);
            }
        })
    };

    Account.uploadPic = function (ctx, options, id_acc, cb) {
        if (!options) options = {};
        ctx.req.params.container = 'common';
        var Container = Account.app.models.container;
        Container.upload(ctx.req, ctx.result, options, function (err, fileObj) {
            if (err) {
                cb(err);
            } else {
                var fileInfo = fileObj.files.file[0],
                    CONTAINERS_URL = '/api/containers/',
                    url = CONTAINERS_URL + fileInfo.container + '/download/' + fileInfo.name;

                Account.findOne({where: {id: id_acc}}, function (err, user) {
                    if (err) cb(err);
                    if (user) {
                        user.image = url;
                        user.save();
                        cb(null, user);
                    }
                })
            }
        })
    };

    Account.loginOrCreateByPhone = function (mobile, cb) {
        Account.findOne({where: {mobile: mobile}}, function (err, user) {
            if (err) return cb(err, user);
            if (user)
                getAccessToken(user, (err, token) => {
                    if (err) return cb(err, user);
                    user.token = token;
                    cb(null, user);
                });
            else
                Account.create({type: 0, email: mobile, mobile: mobile}, (err, user) => {
                    if (err) return cb(err, user);
                    Account.findOne({where: {mobile: mobile}}, function (err, user) {
                        getAccessToken(user, (err, token) => {
                            if (err) return cb(err, user);
                            user.token = token;
                            cb(null, user);
                        })
                    })
                })
        })
    };

    Account.search = function (ctx, data, size, from, type, onDuty, rating, topLeft_lat, topLeft_lng, bottomRight_lat, bottomRight_lng, cb) {
        var terms = [],
            topLeft = {
                lat: topLeft_lat,
                lng: topLeft_lng
            },
            bottomRight = {
                lat: bottomRight_lat,
                lng: bottomRight_lng
            };
        if (1 == onDuty || +0 == onDuty)
            terms.push({term: {"onDuty": onDuty}});
        if (rating != null)
            terms.push({term: {"rating": rating}});
        if (type != null) {
            terms.push({term: {"category.type": type}});
        }

        Account.app.ElasticSearch.search({
            query: data,
            size: size,
            from: from,
            terms: terms,
            topLeft: topLeft,
            bottomRight: bottomRight
        }, cb);
    };

    Account.prototype.publishGeoWatch = function () {
        var app = Account.app,
            clientIds = [];
        if (app.geo_watch && app.geo_watch[this.id])
            clientIds = app.geo_watch[this.id];

        clientIds.forEach((id) => {
            pubsub.publishToAllSessions(app, id, {
                collectionName: 'Account',
                method: 'PATCH',
                data: {geopoint: this.geopoint},
                modelId: this.id
            })
        });
    };

    //TODO: if we use distributed systems on different servers we need some shared real time db like redis
    Account.subscribeGeoWatch = function (watchingIds, observableId) {
        var app = Account.app;
        if (!app.geo_watch) app.geo_watch = {};
        if (!app.geo_watch[observableId]) app.geo_watch[observableId] = [];
        app.geo_watch[observableId] = app.geo_watch[observableId].concat(watchingIds);
        return app.geo_watch;
    };

    Account.unsubscribeGeoWatch = function (watchingId, observableId) {
        var app = Account.app,
            geo_watch = app.geo_watch,
            index_watching;
        if (geo_watch && geo_watch[observableId]) {
            index_watching = geo_watch[observableId].indexOf(watchingId);
            if (index_watching != -1)
                app.geo_watch[observableId].splice(index_watching, 1);
            if (!geo_watch[observableId].length) {
                delete app.geo_watch[observableId]
            }
        }
    };

    Account.prototype.checkBalance = function (cb) {
        var worker = new Worker(CHECKBALANCE);

        this.transactions_received((err, transactions_received) => {
            if (err)  return cb(err, transactions_received);
            this.transactions_sent((err, transactions_sent) => {
                if (err) return cb(err, transactions_sent);
                var account = this.toJSON();
                account.transactions_received = transactions_received;
                account.transactions_sent = transactions_sent;

                worker.exec({name: 'checkAccountsBalance', data: {accounts: [account]}}, (result) => {
                    worker.kill();
                    cb(err, result);
                });
            })
        });
    };

    Account.prototype.getOrdersHistory = function (ctx, skip, limit, cb) {
        var Order = Account.app.models.Order,
            account = this,
            max = 1000000,
            count = 100;

        skip = skip || 0;
        limit = limit || count;

        skip = (skip < max) ? skip : max;
        limit = (limit < max) ? limit : max;

        Order.find({
            skip: skip || 0,
            limit: limit || max,
            where: {
                vendorId: account.id,
                or: [
                    {type: Order.ORDER_STATUS.FINISHED},
                    {type: Order.ORDER_STATUS.CANCELED}
                ]
            }, include: [{
                relation: 'services',
                scope: {
                    include: {
                        relation: 'service_images',
                    }
                }
            }, 'customer', 'vendor']
        }, (err, orders) => {
            if (err) return cb(err);
            cb(null, orders)
        });
    };

    // --- remote endpoints ---
    Account.remoteMethod(
        'OAuth',
        {
            http: {path: '/OAuth', verb: 'POST'},
            accepts: [
                {arg: 'ctx', type: 'object', 'http': {source: 'context'}},
                {arg: 'body', type: 'object', http: {source: 'body'}}
            ],
            returns: {type: 'object', root: true}
        }
    );

    Account.remoteMethod(
        'uploadPic',
        {
            http: {path: '/:id/upload/image', verb: 'POST'},
            accepts: [
                {arg: 'ctx', type: 'object', 'http': {source: 'context'}},
                {arg: 'options', type: 'object', http: {source: 'query'}},
                {arg: 'id', type: 'string'}
            ],
            returns: {arg: 'fileObject', type: 'object', root: true}
        }
    );

    Account.remoteEndpoint(
        'search',
        {
            http: {path: '/search', verb: 'GET'},
            accepts: [
                {arg: 'ctx', type: 'object', 'http': {source: 'context'}},
                {arg: 'data', type: 'string'},
                {arg: 'size', type: 'number'},
                {arg: 'from', type: 'number'},
                {arg: 'type', type: 'number'},
                {arg: 'onDuty', type: 'boolean'},
                {arg: 'rating', type: 'number'},
                {arg: 'topLeft_lat', type: 'string', description: '{lat: 10.32424, lng: 5.84978}'},
                {arg: 'topLeft_lng', type: 'string', description: '{lat: 10.32424, lng: 5.84978}'},
                {arg: 'bottomRight_lat', type: 'string', description: '{lat: 10.32424, lng: 5.84978}'},
                {arg: 'bottomRight_lng', type: 'string', description: '{lat: 10.32424, lng: 5.84978}'}
            ],
            returns: {arg: 'result', type: 'object'}
        }
    );

    Account.prototype.update = function (ctx, data, cb) {
        this.updateAttributes(data, cb);
    };

    Account.remoteEndpoint(
        'update',
        {
            isStatic: false,
            http: {path: '/', verb: 'PATCH'},
            accepts: [
                {arg: 'ctx', type: 'object', 'http': {source: 'context'}},
                {arg: 'data', type: 'Account', http: {source: 'body'}}],
            returns: {type: 'object', root: true}
        }
    );

    Account.remoteEndpoint(
        'withdrawMoney',
        {
            isStatic: false,
            http: {path: '/withdrawMoney', verb: 'POST'},
            accepts: [
                {arg: 'ctx', type: 'object', 'http': {source: 'context'}},
                {arg: 'amount', type: 'number'}
            ],
            returns: {type: 'object', root: true}
        }
    );

    Account.remoteEndpoint(
        '__create__funds',
        {
            isStatic: false,
            http: {path: '/funds', verb: 'POST'},
            accepts: [
                {arg: 'data', type: 'Fund', http: {source: 'body'}}],
            returns: {arg: 'result', type: 'object'}
        }
    );

    Account.remoteEndpoint(
        '__create__cards',
        {
            isStatic: false,
            http: {path: '/cards', verb: 'POST'},
            accepts: [
                {arg: 'data', type: 'CreditCard', http: {source: 'body'}}],
            returns: {arg: 'result', type: 'object'}
        }
    );


    //TODO: check user access!!
    Account.remoteEndpoint(
        '__create__customers',
        {
            isStatic: false,
            http: {path: '/customers', verb: 'POST'},
            accepts: [
                {arg: 'data', type: 'Order', http: {source: 'body'}}],
            returns: {type: 'Order', root: true}
        }
    );

    Account.remoteEndpoint(
        'checkBalance',
        {
            isStatic: false,
            http: {path: '/checkBalance', verb: 'get'},
            accepts: [],
            returns: {type: 'object', root: true}
        }
    );

    Account.remoteEndpoint(
        'getOrdersHistory',
        {
            isStatic: false,
            http: {path: '/ordershistory', verb: 'GET'},
            accepts: [
                {arg: 'ctx', type: 'object', 'http': {source: 'context'}},
                {arg: 'skip', type: 'number'},
                {arg: 'limit', type: 'number'}
            ],
            returns: {type: 'object', root: true}
        }
    );
};
