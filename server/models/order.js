'use strict';
var pubsub       = require('../socket/pubsub.js'),
    duration     = require('../config.json').order.duration,
    ORDER_STATUS = {
	    PENDING: 0,
	    REJECTED: 1,
	    CANCELED: 1,
	    ACCEPTED: 2,
	    ARRIVING: 3,
	    ARRIVED:  4,
	    FINISHED: 5
    };

module.exports = function (Order) {
	//hide remote orders method
	Order.disableRemoteMethod("upsert", true);
	Order.disableRemoteMethod("updateAll", true);
	Order.disableRemoteMethod("updateAttributes", false);

	Order.disableRemoteMethod("deleteById", true);
	Order.disableRemoteMethod("exists", true);

	//hide remote services methods
	Order.disableRemoteMethod('__updateById__services', false);
	Order.disableRemoteMethod('__create__services', false);
	Order.disableRemoteMethod('__delete__services', false);
	Order.disableRemoteMethod('__destroyById__services', false);
	Order.disableRemoteMethod('__link__services', false);
	Order.disableRemoteMethod('__unlink__services', false);

	// --- definitions ---
	Order.definition.properties.created.default = new Date();
	Order.ORDER_STATUS                          = ORDER_STATUS;

	// --- vaildations ---
	Order.validatesPresenceOf('vendorId', 'customerId', 'serviceIds');

	// --- hooks ---
	Order.observe('before save', function (ctx, next) {
		if (!ctx.isNewInstance || !ctx.instance)
			return next();

		var AccountCategory = Order.app.models.AccountCategory,
		    vendorId        = ctx.instance.vendorId,
		    serviceIds      = ctx.instance.serviceIds;

		AccountCategory.find({
			where: {
				and: [
					{accountId: vendorId},
					{onDuty: true}
				]
			},
			include: {
				relation: 'services',
				scope: {
					where: {
						id: {
							inq: serviceIds
						}
					}
				}
			}
		}).then((result) => {
			var ids   = [],
			    count = serviceIds.length;
			if (result.length) {
				result = JSON.parse(JSON.stringify(result));
				result.forEach((item) => {
					if (item && item.services && item.services.length > 0) {
						ids = ids.concat(item.services.map((service) =>
							service.id));
					}
				});
			}
			if (count > 0 && ids.length <= count) {
				serviceIds.forEach((item) => {
					if (ids.indexOf(item) == -1) {
						return next(new Error("Services are unavaliable"));
					}
				});
				ctx.instance.serviceIds = serviceIds;
				return next();
			}
			else
				return next(new Error("Services are unavaliable"));

		}).catch((err) => {
			return next(err);
		});
	});

	Order.observe('after save', function (ctx, next) {
		if (!ctx.instance)
			return next();

		var app                = Order.app,
		    vendorId           = ctx.instance.vendorId,
		    customerId         = ctx.instance.customerId,
		    orderId            = ctx.instance.id,
		    serviceIds         = ctx.instance.serviceIds,
		    service_order_data = [],
		    ServiceOrder       = Order.app.models.ServiceOrder,
		    Account            = Order.app.models.Account;

		if (!ctx.isNewInstance) {
			//unsubscribe account geo watch
			if (ctx.instance.type == ORDER_STATUS.ARRIVED || ctx.instance.type == ORDER_STATUS.CANCELED || ctx.instance.FINISHED)
				Account.unsubscribeGeoWatch(customerId, vendorId);
			//subscribe account geo watch
			if (ctx.instance.type == ORDER_STATUS.ARRIVING)
				Account.subscribeGeoWatch([customerId], vendorId);
			return publishOrder();
		}

		serviceIds.forEach((id) => {
			service_order_data.push({orderId: orderId, serviceId: id})
		});

		ServiceOrder.create(service_order_data, (err, result) => {
			if (err) return next(err);
			publishOrder();
		});

		function publishOrder() {
			Order.findOne({
				where: {
					id: orderId
				},
				include: [{
					relation: 'service_order',
					scope: {
						include: {
							relation: 'service',
							scope: {
								include: 'category'
							}
						}
					}
				}, 'customer', 'vendor']

			}, (err, result) => {
				if (err || !result) return next(err);
				var clientIds = [];
				clientIds.push(vendorId);
				clientIds.push(customerId);

				clientIds.forEach((id) => {
					if (ctx.isNewInstance == true) {
						pubsub.publishToAllSessions(app, id, {
							collectionName: 'Order',
							method: 'POST',
							data: result
						})
					} else {
						pubsub.publishToAllSessions(app, id, {
							collectionName: 'Order',
							method: 'PATCH',
							data: result
						})
					}
				});
				next();
			})
		}
	});

	// --- Order methods ---
	Order.new = function (ctx, cb) {
		var vendorId = ctx.req.accessToken.userId;

		Order.find({
			where: {
				and: [
					{vendorId: vendorId},
					{type: ORDER_STATUS.PENDING},
					{
						created: {
							gt: new Date(Date.now() - duration)
						}
					}
				]
			},
			include: [{
				relation: 'service_order',
				scope: {
					include: {
						relation: 'service',
						scope: {
							include: 'category'
						}
					}
				}
			}, 'customer']
		}, (err, result) => {
			cb(err, result);
		})
	};

	Order.reject = function (ctx, id, cb) {
		var userId = ctx.req.accessToken.userId;

		Order.findOne({
			where: {
				and: [
					{or: [{vendorId: userId}, {customerId: userId}]},
					{type: ORDER_STATUS.PENDING},
					{id: id}
				]
			}
		}, (err, result) => {
			if (err) return cb(err);
			if (result) {
				result.type = ORDER_STATUS.REJECTED;
				result.save((err, res) => {
					cb(err, res);
				});
			}
			else
				cb(err, result);
		})
	};

	Order.accept = function (ctx, id, cb) {
		var AccountCategory = Order.app.models.AccountCategory,
		    vendorId        = ctx.req.accessToken.userId;

		Order.active(ctx, (err, result) => {
			//check active order
			if (result) return cb(new Error('Vendor off duty'), result);
			Order.findOne({
				where: {
					and: [
						{vendorId: vendorId},
						{type: ORDER_STATUS.PENDING},
						{id: id},
						{
							created: {
								gt: new Date(Date.now() - duration)
							}
						}
					]
				}
			}, (err, result) => {
				if (err) return cb(err);
				if (result) {
					result.type = ORDER_STATUS.ACCEPTED;
					AccountCategory.updateAll({accountId: vendorId}, {onDuty: false}, (err, info) => {
						if (err) return cb(err, info);
						result.save((err, res) => {
							cb(err, res);
						});
					});
				}
				else
					cb(err, result);
			})
		})
	};

	Order.arriving = function (ctx, id, date, cb) {
		var vendorId = ctx.req.accessToken.userId;

		Order.findOne({
			where: {
				and: [
					{vendorId: vendorId},
					{or: [{type: ORDER_STATUS.ACCEPTED}, {type: ORDER_STATUS.ARRIVING}]},
					{id: id}
				]
			}
		}, (err, result) => {
			if (err) return cb(err);
			if (result) {
				result.type         = ORDER_STATUS.ARRIVING;
				result.arrival_time = date;
				result.save((err, res) => {
					cb(err, res);
				});
			}
			else
				cb(err, result);
		})
	};

	Order.arrive = function (ctx, id, cb) {
		var vendorId = ctx.req.accessToken.userId;

		Order.findOne({
			where: {
				and: [
					{vendorId: vendorId},
					{type: ORDER_STATUS.ARRIVING},
					{id: id}
				]
			}
		}, (err, result) => {
			if (err) return cb(err);
			if (result) {
				result.type       = ORDER_STATUS.ARRIVED;
				result.start_time = new Date();
				result.save((err, res) => {
					cb(err, res);
				});
			}
			else
				cb(err, result);
		})
	};

	Order.finish = function (ctx, id, cb) {
		var ServiceOrder = Order.app.models.ServiceOrder,
		    vendorId     = ctx.req.accessToken.userId;

		Order.findOne({
			where: {
				and: [
					{vendorId: vendorId},
					{type: ORDER_STATUS.ARRIVED},
					{id: id}
				]
			}
		}, (err, result) => {
			if (err || !result) return cb(err);
			result.type     = ORDER_STATUS.FINISHED;
			result.end_time = new Date();
			result.save((err, res) => {
				if (err) return cb(err);
				ServiceOrder.finishAll(result.id, (err, info) => {
					console.log(err, info);
					cb(err, res);
				})
			});
		})
	};

	Order.cancel = function (ctx, id, cb) {
		var userId = ctx.req.accessToken.userId;

		Order.findOne({
			where: {
				and: [
					{or: [{vendorId: userId}, {customerId: userId}]},
					{or: [{type: ORDER_STATUS.ACCEPTED}, {type: ORDER_STATUS.ARRIVING}]},
					{id: id}
				]
			}
		}, (err, result) => {
			if (err) return cb(err);
			if (result) {
				result.type = ORDER_STATUS.CANCELED;
				result.save((err, res) => {
					cb(err, res);
				});
			}
			else
				cb(err, result);
		})
	};

	Order.startService = function (ctx, orderId, serviceId, cb) {
		var ServiceOrder = Order.app.models.ServiceOrder,
		    vendorId     = ctx.req.accessToken.userId;

		Order.findOne({
			where: {
				and: [
					{vendorId: vendorId},
					{type: ORDER_STATUS.ARRIVED},
					{id: orderId}
				]
			}
		}, (err, result) => {
			if (err || !result) return cb(err);
			ServiceOrder.start(orderId, serviceId, new Date(), cb)
		});
	};

	Order.finishService = function (ctx, orderId, serviceId, cb) {
		var ServiceOrder = Order.app.models.ServiceOrder,
		    vendorId     = ctx.req.accessToken.userId;

		Order.findOne({
			where: {
				and: [
					{vendorId: vendorId},
					{type: ORDER_STATUS.ARRIVED},
					{id: orderId}
				]
			}
		}, (err, result) => {
			if (err || !result) return cb(err);
			ServiceOrder.finish(orderId, serviceId, new Date(), cb)
		});
	};

	Order.active = function (ctx, cb) {
		var userId = ctx.req.accessToken.userId;

		Order.findOne({
			where: {
				and: [
					{or: [{vendorId: userId}, {customerId: userId}]},
					{or: [{type: ORDER_STATUS.ARRIVED}, {type: ORDER_STATUS.ACCEPTED}, {type: ORDER_STATUS.ARRIVING}]}
				]
			},
			include: [{
				relation: 'service_order',
				scope: {
					include: {
						relation: 'service',
						scope: {
							include: 'category'
						}
					},
				}
			}, 'customer', 'vendor']

		}, (err, result) => {
			if (err || !result) return cb(err, result);
			cb(err, result);
		});
	};

	Order.costs = function (ctx, id, cb) {
		var ServiceOrder = Order.app.models.ServiceOrder;
		if (this instanceof Order) {
			this.service_order({include: 'service'}, (err, services) => {
				cb(err, {order: services, cost: calculateCost(services).toFixed(2)});
			})
		} else
			ServiceOrder.find({
				where: {
					orderId: id
				},
				include: {
					relation: 'service'
				}
			}, (err, services) => {
				cb(err, {order: services, cost: calculateCost(services).toFixed(2)});
			});

		function calculateCost(services) {
			var cost = 0;
			services.forEach((item) => {
				var item = item.toJSON();
				if (item.start_time != null && item.end_time != null)
					cost += item.service.cost;
			});
			return cost;
		}
	};

	Order.prototype.costs = function (cb) {
		return Order.costs.call(this, null, null, cb);
	};

	// --- remote endpoints ---
	Order.remoteEndpoint(
		'pay',
		{
			isStatic: false,
			http: {path: '/pay', verb: 'PUT'},
			accepts: [
				{arg: 'ctx', type: 'object', 'http': {source: 'context'}}
			],
			returns: {arg: 'result', root: true}
		}
	);

	Order.remoteEndpoint(
		'active',
		{
			http: {path: '/active', verb: 'GET'},
			accepts: [
				{arg: 'ctx', type: 'object', 'http': {source: 'context'}}
			],
			returns: {arg: 'result', type: 'object'}
		}
	);

	Order.remoteEndpoint(
		'costs',
		{
			http: {path: '/:id/costs', verb: 'GET'},
			accepts: [
				{arg: 'ctx', type: 'object', 'http': {source: 'context'}},
				{arg: 'id', type: 'string'}
			],
			returns: {arg: 'result', type: 'object'}
		}
	);

	Order.remoteEndpoint(
		'startService',
		{
			http: {path: '/:id/service/start', verb: 'PUT'},
			accepts: [
				{arg: 'ctx', type: 'object', 'http': {source: 'context'}},
				{arg: 'id', type: 'string'},
				{arg: 'serviceId', type: 'string'}
			],
			returns: {arg: 'order', type: 'object'}
		}
	);

	Order.remoteEndpoint(
		'finishService',
		{
			http: {path: '/:id/service/finish', verb: 'PUT'},
			accepts: [
				{arg: 'ctx', type: 'object', 'http': {source: 'context'}},
				{arg: 'id', type: 'string'},
				{arg: 'serviceId', type: 'string'}
			],
			returns: {arg: 'order', type: 'object'}
		}
	);

	Order.remoteEndpoint(
		'reject',
		{
			http: {path: '/:id/reject', verb: 'PUT'},
			accepts: [
				{arg: 'ctx', type: 'object', 'http': {source: 'context'}},
				{arg: 'id', type: 'string'}
			],
			returns: {arg: 'order', type: 'object'}
		}
	);

	Order.remoteEndpoint(
		'arrive',
		{
			http: {path: '/:id/arrive', verb: 'PUT'},
			accepts: [
				{arg: 'ctx', type: 'object', 'http': {source: 'context'}},
				{arg: 'id', type: 'string'}
			],
			returns: {arg: 'order', type: 'object'}
		}
	);

	Order.remoteEndpoint(
		'accept',
		{
			http: {path: '/:id/accept', verb: 'PUT'},
			accepts: [
				{arg: 'ctx', type: 'object', 'http': {source: 'context'}},
				{arg: 'id', type: 'string'}
			],
			returns: {arg: 'order', type: 'object'}
		}
	);

	Order.remoteEndpoint(
		'arriving',
		{
			http: {path: '/:id/arriving', verb: 'PUT'},
			accepts: [
				{arg: 'ctx', type: 'object', 'http': {source: 'context'}},
				{arg: 'id', type: 'string'},
				{arg: 'date', type: 'date'}
			],
			returns: {arg: 'order', type: 'object'}
		}
	);

	Order.remoteEndpoint(
		'cancel',
		{
			http: {path: '/:id/cancel', verb: 'PUT'},
			accepts: [
				{arg: 'ctx', type: 'object', 'http': {source: 'context'}},
				{arg: 'id', type: 'string'}
			],
			returns: {arg: 'order', type: 'object'}
		}
	);

	Order.remoteEndpoint(
		'finish',
		{
			http: {path: '/:id/finish', verb: 'PUT'},
			accepts: [
				{arg: 'ctx', type: 'object', 'http': {source: 'context'}},
				{arg: 'id', type: 'string'}
			],
			returns: {arg: 'order', type: 'object'}
		}
	);

	Order.remoteEndpoint(
		'new',
		{
			http: {path: '/new', verb: 'GET'},
			accepts: [
				{arg: 'ctx', type: 'object', 'http': {source: 'context'}}
			],
			returns: {arg: 'orders', type: 'array'}
		}
	);

	Order.remoteEndpoint(
		'__create__review',
		{
			isStatic: false,
			http: {path: '/review', verb: 'POST'},
			accepts: [
				{arg: 'data', type: 'Review', http: {source: 'body'}},
				{arg: 'options', type: 'object', 'http': {source: 'context'}}],
			returns: {arg: 'result', type: 'object'}
		}
	);
};
