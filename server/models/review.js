'use strict';

module.exports = function (Review) {
    Review.disableRemoteMethod("updateAll", true);
    Review.disableRemoteMethod("updateAttributes", true);
    Review.disableRemoteMethod("updateById", true);
    Review.disableRemoteMethod("exists", true);
    Review.disableRemoteMethod("createChangeStream", true);
    Review.disableRemoteMethod("replaceOrCreate", true);
    Review.disableRemoteMethod("replaceById", true);
    Review.disableRemoteMethod("upsertWithWhere", true);

    Review.observe('before save', function (ctx, next) {
        var Order = Review.app.models.Order,
            review = ctx.instance || ctx.currentInstance || ctx.data,
            ownerId = ctx.options && ctx.options.accessToken && ctx.options.accessToken.userId ||
                ctx.options.req && ctx.options.req.accessToken && ctx.options.req.accessToken.userId;

        review.order((err, order) => {
            if (err) return next(err);
            if (!order || order.type !== Order.ORDER_STATUS.FINISHED || order.customerId !== ownerId)
                return next(new Error("Order is incorrect"));
            order.services((err, services) => {
                review.accountId = ownerId;
                review.account_categoryId = services[0].account_categoryId;
                order.review((err, review) => {
                    if (err) return next(err);
                    if (!review) next()
                });
            });
        })
    });
};
