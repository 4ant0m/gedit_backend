'use strict';
var pubsub = require('../socket/pubsub.js');

module.exports = function (AccountCategory) {
	AccountCategory.disableRemoteMethod('__create__reviews', false);
	AccountCategory.disableRemoteMethod('__delete__reviews', false);
	AccountCategory.disableRemoteMethod('__destroyById__reviews', false);
	AccountCategory.disableRemoteMethod('__updateById__reviews', false);
	AccountCategory.disableRemoteMethod('__get__reviews', false);
	AccountCategory.disableRemoteMethod('__delete__reviews', false);
	AccountCategory.disableRemoteMethod('__destroyById__reviews', false);
	AccountCategory.disableRemoteMethod('__updateById__reviews', false);

	AccountCategory.getApp((err, app) => {
			app.on('socket_authenticated', (socket, data) => {
				pubsub.subscribe(socket, {
					collectionName: 'AccountCategory',
					method: 'ONDUTY'
				}, (data) => {
					var id     = data.id,
					    onDuty = data.onDuty,
					    accountId = app.socket_token[socket.id].userId;
					AccountCategory.findOne({ where: { id: id, accountId: accountId} }, (err, result) => {
						if (result) {
							result.onDuty = onDuty;
							result.save();
						}
					})
				})
			});
	});

	AccountCategory.observe('after save', function logQuery(ctx, next) {
		var socket = AccountCategory.app.io;
		if (ctx.isNewInstance == true) {
			pubsub.publish(socket, {
				collectionName: 'AccountCategory',
				method: 'POST',
				data: ctx.instance
			})
		} else {
			if (ctx.instance)
			pubsub.publish(socket, {
				collectionName: 'AccountCategory',
				method: 'PATCH',
				data: ctx.instance,
				modelId: ctx.instance.id
			})
		}
		next();
	});

	AccountCategory.prototype.images = function (ctx, cb) {
		AccountCategory.find({
			where: {
				id: this.id
			},
			include: {
				relation: 'services',
				scope: {
					include: {
						relation: 'service_images'
					}
				}
			}
		}, (err, res) => {
			cb (err, res && res[0] && res[0].toJSON().services)
		})
	};

	AccountCategory.prototype.getReviews = function (ctx, cb) {
		AccountCategory.find({
			where: {
				id: this.id
			},
			include: {
				relation: 'reviews',
				scope: {
					include: {
						relation: 'order'
					}
				}
			}
		}, (err, res) => {
			cb (err, res && res[0] && res[0].toJSON().reviews)
		})
	};

	AccountCategory.remoteEndpoint(
		'images',
		{
			http: {path: '/images', verb: 'GET'},
			isStatic: false,
			accepts: [
				{arg: 'ctx', type: 'object', 'http': {source: 'context'}}
			],
			returns: {type: 'object', root: true}
		}
	);

	AccountCategory.remoteEndpoint(
		'getReviews',
		{
			http: {path: '/reviews', verb: 'GET'},
			isStatic: false,
			accepts: [
				{arg: 'ctx', type: 'object', 'http': {source: 'context'}}
			],
			returns: {type: 'object', root: true}
		}
	);
};
