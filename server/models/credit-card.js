/**
 * Created by 4ant0m on 27.01.17.
 */
'use strict';
var engage = require('../payment').engage;

module.exports = function (CreditCard) {
	CreditCard.disableRemoteMethod("updateAll", true);
	CreditCard.disableRemoteMethod("updateAttributes", true);
	CreditCard.disableRemoteMethod("updateById", true);
	CreditCard.disableRemoteMethod("deleteById", true);
	CreditCard.disableRemoteMethod("exists", true);
	CreditCard.disableRemoteMethod("createChangeStream", true);
	CreditCard.disableRemoteMethod("replaceOrCreate", true);
	CreditCard.disableRemoteMethod("replaceById", true);
	CreditCard.disableRemoteMethod("upsertWithWhere", true);

	CreditCard.observe('before save', function (ctx, next) {
		var Account = CreditCard.app.models.Account;
		if (!ctx.instance) return next();
		Account.findById(ctx.instance.accountId, (err, acc) => {
			if (err || !acc) return next(new Error(err || 'Customer not found'));
			if (acc && acc.stripeId)
				return engage.createCustomerSource(acc.stripeId, ctx.instance)
					.then(res => {
						ctx.instance.stripeId = res.id;
						next()
					})
					.catch(next);

			if (acc && !acc.stripeId)
				engage.createStripeCustomer({email: acc.mail})
					.then(customer => {
						acc.stripeId = customer.id;
						acc.save();
						return engage.createCustomerSource(customer.id, ctx.instance)
					})
					.then(res => {
						ctx.instance.stripeId = res.id;
						next()
					})
					.catch(next)
		})
	});

	CreditCard.observe('before delete', function (ctx, next) {
		CreditCard.find({where: {id: ctx.where.id}, include: [{relation: 'account'}]}, (err, card) => {
			if (err || !card || !card[0]) return next(err);
			card = card[0].toJSON();
			return engage.deleteCustomerCard(card.account.stripeId, card.stripeId)
				.then(res => {
					if (res.deleted == true)
						next();
					else
						next(res)
				})
				.catch(next);
		});
	});

	CreditCard.prototype.setDefault = function (ctx, cb) {
		var Account = CreditCard.app.models.Account;
		var self = this;
		Account.findById(this.accountId, (err, acc) => {
			return engage.setDefaultSource(acc.stripeId, self.sripeId)
			.then(res => cb(null, {
					customerStripeId: res.id,
					default_source: res.default_source,
					account_balance: res.account_balance }
				))
			.catch(cb);
		})
	};

	CreditCard.remoteEndpoint(
		'setDefault',
		{
			isStatic: false,
			http: {path: '/setDefault', verb: 'PUT'},
			accepts: [{arg: 'ctx', type: 'object', 'http': {source: 'context'}}],
			returns: {type: 'object', root: true}
		}
	);
	return this;

};
