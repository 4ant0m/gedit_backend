/**
 * Created by 4ant0m on 21.02.17.
 */
module.exports = function (loopback) {
	//override credentials
	var normalizeCredentials = function (credentials, realmRequired, realmDelimiter) {
		function splitPrincipal(name, realmDelimiter) {
			var parts = [null, name];
			if (!realmDelimiter) {
				return parts;
			}
			var index = name.indexOf(realmDelimiter);
			if (index !== -1) {
				parts[0] = name.substring(0, index);
				parts[1] = name.substring(index + realmDelimiter.length);
			}
			return parts;
		}

		var query   = {};
		credentials = credentials || {};
		if (!realmRequired) {
			if (credentials.mobile) {
				query.mobile = credentials.mobile;
			} else if (credentials.username) {
				query.username = credentials.username;
			}
		} else {
			if (credentials.realm) {
				query.realm = credentials.realm;
			}
			var parts;
			if (credentials.mobile) {
				parts        = splitPrincipal(credentials.mobile, realmDelimiter);
				query.mobile = parts[1];
				if (parts[0]) {
					query.realm = parts[0];
				}
			} else if (credentials.username) {
				parts          = splitPrincipal(credentials.username, realmDelimiter);
				query.username = parts[1];
				if (parts[0]) {
					query.realm = parts[0];
				}
			}
		}
		return query;
	};
	loopback.User.normalizeCredentials = normalizeCredentials;
};