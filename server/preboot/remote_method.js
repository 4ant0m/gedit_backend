/**
 * Created by 4ant0m on 13.01.17.
 */
'use strict';

var pubsub = require('../socket/pubsub.js');

module.exports = function (loopback) {
	function remoteEndpoint(method, option) {
		option.collectionName = option.collectionName || this.sharedClass.name;
		this.remoteMethod.apply(this, arguments);
		this.getApp((err, app) => {
			app.setMaxListeners(app.models.length);
			app.on('socket_authenticated', (socket, data) => {
				var reg                 = /[A-Z]/,
				    mark                = '__',
				    markLength          = mark.length,
				    indexUpperCase      = method.search(reg),
				    indexUnderLineLast  = method.lastIndexOf(mark),
				    indexUnderLineFirst = method.indexOf(mark),
				    action              = method.substr(0, indexUpperCase);
				if (indexUpperCase != -1)
					action = method.substr(indexUpperCase).toLowerCase() + '/' + action.toUpperCase();
				else
					action = method.toUpperCase();

				if (indexUnderLineLast != -1)
					action = method.substr(indexUnderLineLast + markLength).toLowerCase() + '/' + method.substring(indexUnderLineFirst + markLength, indexUnderLineLast).toUpperCase();

				if (method.substr(indexUnderLineLast + markLength).length == 0)
					action = method.substring(indexUnderLineFirst + markLength, indexUnderLineLast).toUpperCase();

				action = action.replace('UPDATE', 'PATCH');
				action = action.replace('CREATE', 'POST');

				pubsub.subscribe(socket, {
					collectionName: option.collectionName,
					method: action
				}, (data) => {
					var accessToken   = app.socket_token[socket.id];
					data.ctx          = {
						req: {
							accessToken: accessToken
						}
					};
					data.options = {
						accessToken: accessToken
					};
					var args          = option.accepts.map((item) =>
							data[item.arg]
					);
					args[args.length] = (err, result) => {
						if (err)
							err = {
								name: err.name,
								message: err.message
							};
						pubsub.publish(socket, {
							collectionName: option.collectionName,
							data: err || result,
							method: action,
							modelId: data.id
						});
					};
					if (option.isStatic === false) {
						this.findById(data.id, (err, result) => {
							if (err || !result) return args[args.length - 1](err, result);
							result[method].apply(result, args);
						})
					} else
						this[method].apply(this, args);
				});
			});
		});
	}

	loopback.PersistedModel.remoteEndpoint = remoteEndpoint;
	loopback.User.remoteEndpoint           = remoteEndpoint;
};
