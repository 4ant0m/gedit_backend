/**
 * Created by 4ant0m on 14.02.17.
 */
'use strict';

const Dwolla = require('../utils').dwolla;
const PARAMS = {
    key: 'FlU27FbwmpD1BQbJdYXH2nFoP7WxlxylKsMJURnukiootGbmnE',
    secret: '3tOKOOdR8CqZqD4omVdjt23zIPAZurdALIwUBxyZbVwevJC9hi',
    environment: 'sandbox'
};
var log = console.log;

class Withdraw {
    constructor(option) {
        var self = this;
        this.dw = new Dwolla(option || PARAMS);
        this.getFunds().then(funds => {
            var balance = funds.filter((fund) => {
                return fund.type == 'balance'
            });
            self.systemFundId = balance[0].id;
        })
    }

    createFund(data) {
        return this.dw.createFund(data)
    }

    getFunds() {
        return new Promise((resolve, reject) => {
            this.dw.getAccount().then(account => this.dw.getFunds(account.id)).then(resolve).catch(reject);
        });
    }

    getBalance() {
        return new Promise((resolve, reject) => {
            this.getFunds().then(funds => {
                var balance = funds.filter((fund) => {
                    return fund.type == 'balance'
                });
                return this.dw.getFundBalance(balance[0].id)
            }).then(resolve).catch(reject);
        })
    }

    getCustomerFunds(id) {
        return this.dw.getCustomerFunds(id)
    }

    removeAllFunds() {
        return new Promise((resolve, reject) => {
            this.getFunds().then(funds => funds.forEach(fund => this.dw.removeFund(fund.id).then(resolve).catch(reject))).catch(reject)
        })
    }

    getCustomers() {
        this.dw.getCustomers().then(res => log(JSON.stringify(res, '', 4)));
    }

    withdrawMoney(sourceId, customerId, amount) {
        return this.dw.createTransfer(sourceId, customerId, amount).then(res => this.dw.getTransfer(res))
    };

    createDwollaUser(user) {
        return new Promise((resolve, reject) => {
            if (!user) reject();
            if (user.dwollaId) resolve(user.dwollaId);
            this.dw.createCustomer({
                firstName: user.first_name,
                lastName: user.last_name,
                email: user.mail,
                type: "receive-only"
            })
                .then(resolve)
                .catch(reject)
        })
    }

    createCustomerFund(customerId, data) {
        return this.dw.createCustomerFund(customerId, {
            "routingNumber": data.routingNumber,
            "accountNumber": data.accountNumber,
            "type": (data.checking === true) ? 'checking' : 'savings',
            "name": data.name
        });
    }

    createSystemFund(data) {
        return this.dw.createFund({
            "routingNumber": data.routingNumber,
            "accountNumber": data.accountNumber,
            "type": (data.checking === true) ? 'checking' : 'savings',
            "name": data.name
        });
    }

    removeSystemFund(id) {
        return this.dw.removeFund(id)
    }

    updateSystemFund(data) {
        return this.dw.updateFund(data.id, data)

    }
}

module.exports = Withdraw;