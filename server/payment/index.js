/**
 * Created by 4ant0m on 16.02.17.
 */
'use strict';
const mediator = require('./mediator');

module.exports = mediator;

module.exports.init = function (data) {
	module.exports = new mediator(data)
};