/**
 * Created by 4ant0m on 24.02.17.
 */
'use strict';

const Stripe = require('../utils').stripe;
const PARAMS = {
	key: 'sk_test_2ogevCWB8i6UQoUfn2CEywgM'
};
var log      = console.log;

class Engage {
	constructor(option) {
		this.st = new Stripe(option || PARAMS);
		return this;
	}

	charge(amount, customer) {
		return this.st.charges.create({
			amount: amount * 100,
			currency: 'usd',
			customer: customer
		});
	}

	createStripeCustomer(data) {
		return this.st.customers.create(data)
	}

	createCustomerSource(id, data) {
		return this.st.customers.createSource(id, {
			source: {
				object: 'card',
				exp_month: data.exp_month,
				exp_year: data.exp_year,
				number: data.number,
				cvc: data.cvc
			}
		});
	}

	deleteCustomerCard(customerId, cardId){
		return this.st.customers.deleteCard(customerId, cardId)
	}

	setDefaultSource(customerId, sourceId){
		return this.st.customers.update(customerId, {
			default_source: sourceId
		});
	}

	test() {
		var self = this;
		this.st.customers.create({
			email: 'foo-customer@example.com'
		}).then(function (customer) {
			return self.st.customers.createSource(customer.id, {
				source: {
					object: 'card',
					exp_month: 10,
					exp_year: 2018,
					number: '4242 4242 4242 4242',
					cvc: 100
				}
			});
		}).then(function (source) {
			log(source);
			return self.st.charges.create({
				amount: 1600,
				currency: 'usd',
				customer: source.customer
			});
		}).then(function (charge) {
			log(charge);
			// New charge created on a new customer
		}).catch(function (err) {
			log(err);
			// Deal with an error
		});
	}
}

module.exports = Engage;