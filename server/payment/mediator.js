/**
 * Created by 4ant0m on 27.02.17.
 */

'use strict';

var withdraw = require('./withdraw'),
    engage = require('./engage'),
    Big = require('big.js');

withdraw = new withdraw();
engage = new engage();

class Mediator {
    constructor(data) {
        var self = this,
            models = data.models;
        this.fee = data.fee;
        this.withdraw = withdraw;
        this.engage = engage;
        this.Transaction = models.Transaction;
        this.Account = models.Account;
        this.Order = models.Order;
        this.SystemMoney = models.SystemMoney;
        this.CreditCard = models.CreditCard;
        this.Fund = models.Fund;

        this.Account.prototype.withdrawMoney = function (ctx, amount, cb) {
            var balance = new Big(this.balance);
            if (this.balance < amount)
                return cb(new Error("Insufficient funds"));
                self.withdrawMoney(amount, this.dwollaId)
                    .then(res => {
                        this.balance = balance.minus(amount);
                        this.save();
                        cb(null, res)
                    })
                    .catch(err => cb(err, null));
        };

        this.Account.prototype.chargeAccount = function (ctx, amount, cb) {
            self.chargeAccount(this, amount)
                .then(res => cb(null, res))
                .catch(cb)
        };

        this.Account.prototype.changeAccountBalance = function (ctx, amount, fee, cb) {
            if (fee === null)
                fee = self.fee;
            if (amount <= 0) fee = 0;
            self.changeAccountBalance(this, amount, fee)
                .then(res => cb(null, res))
                .catch(cb)
        };

        this.Order.prototype.pay = function (ctx, cb) {
            this.costs((err, res) => {
                if (err) return cb(err);
                self.pay(this.customerId, this.vendorId, res.cost, this.id)
                    .then(res => cb(null, res))
                    .catch(cb)
            })
        };

        this.SystemMoney.changeSystemMoneyBalance = function (cb) {
            self.changeSystemMoneyBalance().then(res => cb(null, res)).catch(cb);
        };
        this.SystemMoney.remoteMethod(
            'changeSystemMoneyBalance',
            {
                http: {path: '/changeSystemMoneyBalance', verb: 'GET'},
                accepts: [],
                returns: {type: 'object', root: true}
            }
        );
        return this;
    }

    pay(senderId, recipientId, cost, orderId) {
        return new Promise((resolve, reject) => {
            this._getAccount(senderId).then(sender => {
                if (cost > sender.balance) {
                    this.chargeAccount(sender, cost)
                        .then(this.sendMoney(senderId, recipientId, cost, orderId)
                            .then(resolve)
                            .catch(reject))
                } else
                    this.sendMoney(senderId, recipientId, cost, orderId)
                        .then(resolve)
                        .catch(reject)
            })
        });
    }

    _getAccount(accountId) {
        return new Promise((resolve, reject) => {
            this.Account.findById(accountId, (err, account) => {
                if (err || !account) reject(err || 'User not found');
                resolve(account)
            })
        });
    }

    _createTransaction(senderId, recipientId, cost, fee, orderId) {
        return this.Transaction.create({
            "cost": cost,
            "orderId": orderId,
            "senderId": senderId,
            "fee": fee,
            "recipientId": recipientId
        })
    }

    sendMoney(senderId, recipientId, cost, orderId) {
        return new Promise((resolve, reject) => {
            var fee = this.fee;
            this._createTransaction(senderId, recipientId, cost, fee, orderId)
                .then(transaction =>
                    transaction.send())
                .then(resolve)
                .catch(reject)
        })
    }

    chargeAccount(account, cost) {
        return new Promise((resolve, reject) => {
            this._createTransaction(null, account.id, cost, 0)
                .then(transaction => {
                    this.engage.charge((cost), account.stripeId)
                        .then(transaction.send())
                        .then(resolve)
                        .catch(reject)
                }).catch(reject)
        });
    }

    changeAccountBalance(account, cost, fee) {
        return new Promise((resolve, reject) => {
            var balance = new Big(account.balance),
                diff = new Big(cost);
            fee = fee || 0;
            fee = new Big(1 - fee || 0);
            diff = diff.mul(fee);
            balance = balance.plus(diff);
            account.balance = balance;
            account.save()
                .then(resolve)
                .catch(reject);
        })
    }

    withdrawMoney(amount, customerId, sourceId) {
        return new Promise((resolve, reject) => {
            console.log('fund', this.withdraw.systemMoney);
            sourceId = sourceId || this.withdraw.systemFundId;
            this.withdraw.withdrawMoney(sourceId, customerId, amount)
                .then(resolve)
                .catch(reject)
        })
    }

    changeSystemMoneyBalance() {
        return new Promise((resolve, reject) => {
            this.withdraw.getBalance()
                .then(res => {
                    this.SystemMoney.findById(1, (err, systemMoney) =>{
                        if (err || !systemMoney) reject(err);
                        systemMoney.balance = res.balance.value;
                        systemMoney.save((err, res) => {
                            if (err) reject(err);
                            resolve(systemMoney);
                        })
                    })
                })
                .catch(reject)
        })

    }
}

module.exports = Mediator;
module.exports.withdraw = withdraw;
module.exports.engage = engage;