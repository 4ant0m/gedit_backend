/**
 * Created by 4ant0m on 28.02.17.
 */

'use strict';
var payment = require('../payment');

module.exports = function (server) {
    // init payment
    var data = {
        models: server.models,
        fee: server.get('payment').gedit_profit
    };

    payment.init(data)
};
