'use strict';

module.exports = function (app) {
	var pubsub = require('../socket/pubsub.js');

	//--- socket events ---
	app.on('socket_authenticated', (socket, data) => {
		// --- Time/CURRENT ---
		pubsub.subscribe(socket, {
			collectionName: 'Time',
			method: 'CURRENT'
		}, (data) => {
			pubsub.publish(socket, {
				collectionName: 'Time',
				data: new Date(),
				method: 'CURRENT'
			})
		});
	});
};
