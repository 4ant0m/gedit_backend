'use strict';

module.exports = function (server) {
	server.dataSources['pq'].setMaxListeners(server.models.length);
	if (process.env.FORCE_MIGRATE != 'false' && process.env.FORCE_MIGRATE == 'true' || server.get('force_migrate') === true)
		server.dataSources['pq'].automigrate(null, function (err) {
			if (err)
				console.log(err);
			return err;
		});
	else {
		if (server.get('auto_update') === true) {
			server.dataSources['pq'].autoupdate(null, function (err) {
				if (err)
					console.log(err);
				return err;
			});
		}
	}
};
