/**
 * Created by 4ant0m on 04.01.17.
 */
'use strict';

var logger          = require('../utils').logger;
let WATCH_MODELS    = ['Account', 'AccountCategory', 'Service'],
    SYNC_OPTION     = {
	    model: 'AccountCategory',
	    option: {
		    include: [
			    {relation: 'account'},
			    {relation: 'services'},
			    {relation: 'category'}
		    ]
	    }
    },
    SEARCH_FIELDS   = [
	    "account.*_name",
	    "account.mobile",
	    "category.name",
	    "services.title",
	    "services.description"
    ],
    EXCLUDES_FIELDS = ["account.account_number", "account.router_number", "account.password", "account.credentials", "account.challenges", "account.created", "account.ssn_encrypted", "account.lastUpdated", "account.realm"],
    MAPPING_FIELDS  = {
	    account: {
		    "properties": {
			    geopoint: {
				    type: "geo_point"
			    }
		    }
	    }
    },
    GEOPOINT_FIELD  = "account.geopoint",
    SIZE_LIMIT      = 1000;

module.exports = function (server) {
	var elasticsearch = require('../utils').elasticsearh,
	    config        = server.get('elastic_search'),
	    ElasticSearch = new elasticsearch(config, {
		    SEARCH_FIELDS: SEARCH_FIELDS,
		    EXCLUDES_FIELDS: EXCLUDES_FIELDS,
		    GEOPOINT_FIELD: GEOPOINT_FIELD,
		    MAPPING_FIELDS: MAPPING_FIELDS,
		    TYPE: SYNC_OPTION.model.toLowerCase(),
		    SIZE_LIMIT: SIZE_LIMIT
	    });

	server.ElasticSearch = ElasticSearch;

	function syncData(option, model) {
		option = option || {};
		server.models[model].find(option).then((result) => {
			logger.debug(JSON.stringify(result, "", 4));
			if (result.length)
			ElasticSearch.syncData(result, model.toLowerCase(), (err, response) => {
				if (err) return logger.error(err);
				logger.info(response);
			});
		});
	}

	function getSearchFields(models) {
		var fields = [];

		models.forEach((i)=> {
			var properties = server.models[i].definition.properties;
			for (var field in properties) {
				if (properties.hasOwnProperty(field))
					fields.push(field);
			}
		});
		return fields;
	}

	if (server.get('auto_update') === true) {
		syncData(SYNC_OPTION.option, SYNC_OPTION.model);
	}

	WATCH_MODELS.forEach((item) => {
		var accountId,
		    option = {
			    where: {
				    accountId: null
			    }
		    };

		server.models['Service'].observe('before delete', function (ctx, next) {
			server.models['Service'].findById(ctx.where.id, (err, result) => {
				if (err) return next (err);
				if (result)
					result.account_category((err, result) => {
						ctx.where.account_category = result;
						next();
					});
				else
					next();
			})
		});

		server.models[item].observe('after delete', function (ctx, next) {
			if (ctx.where.account_category)
				ctx.where.account_category.save();
			next();
		});

		server.models[item].observe('after save', function (ctx, next) {
			if (ctx.instance) {
				if (item == 'Account')
					accountId = ctx.instance.id;
				else
					accountId = ctx.instance.accountId;
				option.where.accountId = accountId;
				if (ctx.instance.categoryId)
					option.where.categoryId = ctx.instance.categoryId;
				option = Object.assign(option, SYNC_OPTION.option);
				syncData(option
					, SYNC_OPTION.model);
			}
			next();
		});
	});
};
