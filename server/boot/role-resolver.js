/**
 * Created by 4ant0m on 01.02.17.
 */
'use strict';

module.exports = function(app) {
	var Role = app.models.Role;

	Role.registerResolver('ownerOrder', function(role, context, cb) {
		function reject() {
			process.nextTick(function() {
				cb(null, false);
			});
		}

		// if the target model is not order
		if (context.modelName !== 'order') {
			return reject();
		}

		// do not allow anonymous users
		var userId = context.accessToken.userId;
		if (!userId) {
			return reject();
		}

	});
};