/**
 * Created by 4ant0m on 28/04/17.
 */
'use strict';
var Mailgun = require('../utils').mailgun;

module.exports = function enableAuthentication(server) {
    var mailgun = new Mailgun(server.get('mailgun')).client;

    var data = {
        from: 'Excited User <me@samples.mailgun.org>',
        to: 'tet@ru',
        subject: 'Hello',
        text: 'Testing some Mailgun awesomness!'
    };

    mailgun.messages().send(data, function (err, body) {
        if (err) console.error(err);
        console.log('body - ', body);
    });
};
