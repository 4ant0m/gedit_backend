
#! /bin/bash
#	Copyright 2015-2016, Google, Inc.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# [START startup]
set -v

# Talk to the metadata server to get the project id
PROJECTID=$(curl -s "http://metadata.google.internal/computeMetadata/v1/project/project-id" -H "Metadata-Flavor: Google")

# Install logging monitor. The monitor will automatically pick up logs sent to
# syslog.
# [START logging]
curl -s "https://storage.googleapis.com/signals-agents/logging/google-fluentd-install.sh" | bash
service google-fluentd restart &
# [END logging]

apt-get update
yes Y | apt-get upgrade 

# Install node.js, git, ca, supervisor, build-essential
yes Y | apt-get install -yq ca-certificates git nodejs build-essential supervisor 
mkdir /opt/nodejs
curl https://nodejs.org/dist/v4.2.2/node-v4.2.2-linux-x64.tar.gz | tar xvzf - -C /opt/nodejs --strip-components=1
ln -s /opt/nodejs/bin/node /usr/bin/node
ln -s /opt/nodejs/bin/npm /usr/bin/npm
yes Y | apt-get install npm 

# Install nginx
yes Y | apt-get install nginx 
update-rc.d nginx defaults

# Install Java
yes Y | apt-get install openjdk-7-jre
yes Y | add-apt-repository -y ppa:webupd8team/java
yes Y | apt-get -y install oracle-java8-installer

# Install elasticsearch 
wget https://download.elastic.co/elasticsearch/elasticsearch/elasticsearch-2.4.1.deb
dpkg -i elasticsearch-2.4.1.deb
update-rc.d elasticsearch defaults

service elasticsearch start

# Install postgres
yes Y | apt-get install postgresql postgresql-contrib

# Get the application source code from the Google Cloud Repository.
# git requires $HOME and it's not set during the startup script.
export HOME=/root
git config --global credential.helper gcloud.sh
git clone https://source.developers.google.com/p/$PROJECTID /opt/app

# Install app dependencies
cd /opt/app/
npm install

cd /opt/app/front
npm install
npm run production

# Create a nodeapp user. The application will run as this user.
useradd -m -d /home/nodeapp nodeapp
chown -R nodeapp:nodeapp /opt/app

# Export environment variables
export NODE_PATH=.
# Configure supervisor to run the node app.
cat >/etc/supervisor/conf.d/node-app.conf << EOF
[program:nodeapp]
directory=/opt/app/
command=npm start
autostart=true
autorestart=true
user=nodeapp
environment=HOME="/home/nodeapp",USER="nodeapp",NODE_ENV="production",PORT=3000, DB_HOST="localhost",DB_USER="postgres", DB="postgres", DB_PASSWORD="postgres", DB_PORT="5432", DB_ELASTIC_HOST='localhost',DB_ELASTIC_PORT=9200, DB_ELASTIC_DB='gedit'

stdout_logfile=/var/log/log_nodeapp
stderr_logfile=/var/log/errlog_nodeapp
EOF

supervisorctl reread
supervisorctl update

cat >/etc/nginx/conf.d/sites-enabled/default << EOF

                 # Node.js Settings
                 ##

                 upstream backend {
                                 server localhost:3000;
                 }

                 server {
                         # Listen IP
                          listen 80;
                          server_name *;

                          location / {

                                 # IP node.js process
                                 proxy_pass  http://backend
                                 proxy_set_header Host \$host;
                          }

                          location ~* .(jpg|jpeg|gif|png|ico|css|zip|rar|pdf|js|html|svg)$ {

                                 # Path to static files
                                 root /opt/app/public;
                                 access_log   off;
                                 try_files \$uri \$uri/ @nextserver;
                          }

                          location @nextserver {
                                          proxy_pass           http://nextserver;
                                          proxy_connect_timeout      70;
                                          proxy_send_timeout         90;
                                          proxy_read_timeout         90;
                          }
                 }


EOF

service nginx restart

# Application should now be running under supervisor
# [END startup]
